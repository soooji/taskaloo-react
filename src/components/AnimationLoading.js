import React from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";

function AnimationLoading({ className, ...props }) {
  return <div className={className + " loading"}></div>;
}

const StyledAnimationLoading = styled(AnimationLoading)`
  -webkit-font-smoothing: antialiased;
  width: ${(props) => props.width ?? "auto"};
  height: ${(props) => props.height ?? "153px"};
  margin-right: 20px;
  margin-bottom: 20px;
  border-radius: ${CONSTS.borderRadius.outside};
  background-color: ${CONSTS.colors.grey[9]};
`;
export default StyledAnimationLoading;
