import React from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { Flex, Icon } from "./";
import { Link } from "react-router-dom";

function GroupCard({ className, title, description, defaultIcon, link,linkto, ...props }) {
  return (
    <Link to={link} className={className + " noselect"} {...props}>
      <Flex className="container" fd="column" ai="flex-start" jc="flex-start">
        {/* {icon ? (
          <img src={CONSTS.baseMedia+icon} className="group-icon-image" alt="تصویر گروه"/>
        ) : (
          <Icon className={"group-icon"} name={defaultIcon} larg="true" size="50px" />
        )} */}
        <div className={"group-name"}>{title}</div>
        <div className={"group-description"}>{description}</div>
        <div className="link-style">
          <Icon
          name="back"
          color="black"
          />
        </div>
      </Flex>
    </Link>
  );
}
const StyledGroupCard = styled(GroupCard)`
  /* display: table; */
  padding:30px;
  box-sizing:border-box;
  border: 1px solid ${CONSTS.colors.grey[8]};
  background: #ffffff;
  box-shadow: 0 6px 32px 0 rgba(0, 0, 0, 0.03);
  border-radius: 15px;
  min-height: 200px;
  /* min-width: 256px; */
  margin-bottom: 20px;
  margin-right: 20px;
  cursor: pointer;
  transition: ease background 0.2s, ease transform 0.2s, ease border 0.2s;
  /* width: calc(20% - 0px); */
  /* &:last-child:not(:nth-child(4n)) {
    margin-left: auto;
  } */
  /* 
  &:nth-child(6n),
  &:nth-child(1) {
    margin-right: 0;
  } */
  .link-style{
    margin-top: auto;
    border-radius: 10px;
    padding:7px;
    margin-right:auto;
    background: ${CONSTS.colors.success.light};
    display: flex;
    align-items: center;
    justify-content: center;
    height: 30px;
    width: 30px;
    color: ${CONSTS.colors.success.normal};
    border: 1px solid ${CONSTS.colors.success.midLight};
  }
  .container{
    height: 100%;
  }

  .group-icon {
    margin-top: 35px;
    color: black;
  }
  .group-icon-image {
    margin-top: 35px;
    width: auto;
    height: 50px;
    object-fit: contain;
    object-position: center;
  }
  .group-name {
    /* margin-top: 23px; */
    font-family: ${CONSTS.fonts.normal};
    font-weight: 500;
    font-size: 16px;
    color: #000000;
    /* text-align: center; */
  }
  .group-description {
    margin-top: 13px;
    font-family: ${CONSTS.fonts.normal};
    font-weight: 400;
    font-size: 14px;
    color: #000000;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
    /* text-align: center; */
  }
  &:hover {
    background: ${CONSTS.colors.info.light};
    border-color: ${CONSTS.colors.info.normal};
    transform: scale(1.05);
  }
  &:active {
    transform: scale(0.98);
  }
`;

export default StyledGroupCard;
