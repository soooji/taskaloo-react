import React, { useState, useEffect, useContext } from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { getCachedUser } from "../utils/functions";
import { Button, Flex, UserAvatar } from "./";
import SystemLogo from "./../static/images/taskaloo.svg";
import { Link } from "react-router-dom";
import { NavBarContext } from "../utils/baseContext";

function Header({ className, ...props }) {
  //-- STATES --//
  const [user, setUser] = useState(null);

  //-- HOOKS --//
  const navbar = useContext(NavBarContext);
  useEffect(() => {
    getUser();
  }, []);

  //-- OTHER FUNCTIONS --//
  const getUser = async () => {
    let usr = await getCachedUser();
    setUser(usr);
  };
  const logOut = async () => {
    if (typeof Storage !== "undefined") {
      localStorage.clear();
    }
    window.location.replace("/auth");
  };

  return (
    <Flex jc="space-between" ai="center" className={className} {...props}>
      <div onClick={() => navbar.toggleMenu()} className="menu-icon-container">
        <div className={`menu-icon ${navbar.isMenuOpen ? "opened" : ""}`}>
          <span></span>
        </div>
      </div>
      <StyledLogo />
      <Flex jc="flex-end" ai="center">
        {user?.first_name || user?.last_name ? (
          <Link to="/profile">
            <UserAvatar fullName={`${user?.first_name} ${user?.last_name}`} />
          </Link>
        ) : null}
        <Button
          info={1}
          invert={1}
          onClick={logOut}
          nomargin={1}
          style={{ marginRight: 10, borderRadius: 30, width: 44 }}
          icon="shutdown"
          iconStyle={{ margin: 0 }}
        ></Button>
      </Flex>
    </Flex>
  );
}

const StyledHeader = styled(Header)`
  width: 100%;
  height: 64px;
  background: #ffffff;
  box-shadow: 0 4px 71px 0 rgba(0, 0, 0, 0.07);
  padding-left: 25px;
  padding-right: 25px;
  box-sizing: border-box;
  position: fixed;
  top: 0;
  right: 0;
  z-index: 111;
  @media only screen and (max-width: ${CONSTS.responsive.md}px) {
    padding-left: 15px;
    padding-right: 15px;
  }
  .menu-icon-container {
    padding: 10px;
    @media only screen and (min-width: ${CONSTS.responsive.md}px) {
      display: none;
    }
  }
  .menu-icon {
    width: 23px;
    height: 17px;
    transition: 1s all;
    transform-origin: 50% 50%;
    transform: rotate(0deg);
  }

  .menu-icon span,
  .menu-icon:before,
  .menu-icon:after {
    content: "";
    width: 100%;
    height: 15%;
    background: ${CONSTS.colors.grey[5]};
    position: absolute;
    border-radius: 10px;
    transition: 0.8s all;
    transform-origin: 100% 100%;
    left: 0;
  }

  .menu-icon:before {
    transform-origin: 0% 0%;
    top: 0;
  }

  .menu-icon span {
    top: 43%;
  }

  .menu-icon:after {
    top: 85%;
  }

  .menu-icon.opened {
    transform: rotate(360deg);
  }

  .menu-icon.opened:before {
    transform: rotate(45deg);
    left: 20%;
    top: -8%;
  }

  .menu-icon.opened span,
  .menu-icon.opened:after {
    transform: rotate(-45deg);
    left: -10%;
    top: -12%;
  }
`;

//logo
function Logo({ className, ...props }) {
  return (
    <Flex jc="center" ai="center" className={className} {...props}>
      <div className="logo-img"></div>
      <Flex
        fd="column"
        ai="flex-start"
        jc="space-between"
        className="logo-texts"
      >
        <div className="logo-texts__title">تسکالو</div>
        <div className="logo-texts__sub-title">سیستم مدیریت تسکاتون</div>
      </Flex>
    </Flex>
  );
}

const StyledLogo = styled(Logo)`
  width: auto;
  height: auto;
  .logo-img {
    background-image: url(${SystemLogo});
    background-repeat: no-repeat;
    background-size: contain;
    width: 38px;
    height: 38px;
  }
  .logo-texts {
    margin-right: 15px;
    @media only screen and (max-width: ${CONSTS.responsive.sm}px) {
      display: none;
    }
    &__title {
      font-family: ${CONSTS.fonts.normal};
      font-weight: 500;
      font-size: 16px;
      color: #000000;
    }
    &__sub-title {
      font-family: ${CONSTS.fonts.normal};
      font-weight: 300;
      font-size: 12px;
      color: #000000;
      margin-top: 2px;
    }
  }
`;
export default StyledHeader;
