import React from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { Button, ButtonGroup, Flex } from "./";
import { Modal } from "react-responsive-modal";

const ConfirmModal = ({
  className,
  title,
  message,
  onClose,
  onAccept,
  isLoading = false,
}) => {
  const confirm = async () => {
    await onAccept();
    onClose();
  };
  return (
    <Modal
      open={true}
      onClose={onClose}
      center
      classNames={{
        modal: `normal-modal__container ${className} normal-modal__confirm-container`,
        overlay: "normal-modal__overlay",
        closeButton: "normal-modal__close-button",
      }}
    >
      <Flex className="modal-title-bar">
        <div className="modal-title-bar__text">{title}</div>
      </Flex>
      {message ? <div className="message">{message}</div> : null}
      <ButtonGroup style={{ marginTop: 15 }}>
        <Button
          onClick={confirm}
          error
          icon="trash_can"
          isLoading={isLoading ? 1 : 0}
          disabled={isLoading}
        >
          بله، حذف مورد
        </Button>
        <Button
          info={1}
          invert={1}
          onClick={onClose}
          disabled={isLoading}
        >
          خیر، انصراف
        </Button>
      </ButtonGroup>
    </Modal>
  );
};
const StyledConfirmModal = styled(ConfirmModal)`
  font-family: ${CONSTS.fonts.normal};
  .message {
    font-size: 0.9rem;
  }
`;

export default StyledConfirmModal;
