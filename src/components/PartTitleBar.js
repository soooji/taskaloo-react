import React from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { Flex } from "./";

function PartTitleBar({ className, title, meta, ...props }) {
  return (
   <Flex className={className} ai="center" jc="space-between" {...props}>
      <div className="part-title">
         {title}
      </div>
      <div className="part-meta">
         {meta}
      </div>
   </Flex>
  );
}
const StyledPartTitleBar = styled(PartTitleBar)`
  border-bottom: 1px solid ${CONSTS.colors.grey[8]};
  border-top: 1px solid ${CONSTS.colors.grey[8]};
  padding: 13px 30px;
  width: 100%;
  box-sizing: border-box;
  .part-title {
   font-family: ${CONSTS.fonts.normal};
   font-weight: 500;
   font-size: 16px;
  }
`;

export default StyledPartTitleBar;
