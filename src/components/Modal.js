import React from 'react';
import styled from 'styled-components';
import CONSTS from '../utils/constants';

const Overlay = styled.div`
    position: fixed;
    z-index:11111111111;
    left:0;
    right:0;
    bottom:0;
    top:0;
    width: 100%;
    height: 100%;
    background:rgba(0, 0, 0, .3);
    opacity: 0;
    transition: opacity .2s;
    pointer-events: none;
    &.active{
        opacity: 1;
        pointer-events: all;
    }
`;


const Modal = ({className, title, children, active, onToggle, removeHeader, ...rest}) => {
    
    return (
        <>
            <Overlay className={active && "active"} onClick={onToggle}/>
            <div className={`${className} ${active && "active"}`} {...rest}>
                <div className="_overlay"></div>
                {!removeHeader &&
                <div className="_header">
                    <div className="_title">{title}</div>
                    <div className="_close" onClick={onToggle}>
                        <i className="icon-delete_sign"></i>
                    </div>
                </div>}
                <div className="_body">
                    {children}
                </div>
            </div>
            
        </>
    )
}
const StyledModal = styled(Modal)`
    position: fixed;
    left: 50%;
    top:50%;
    transform:translate(-50%, -50%) scale(.5);
    width: 400px;
    max-height: 90vh;
    opacity:0;
    background: #fff;
    border-radius: ${CONSTS.borderRadius.normal};
    z-index:111111111111;
    transition: opacity .1s, transform .2s;
    transition-timing-function:ease-out;
    overflow: auto;
    pointer-events: none;
    box-shadow: 0px 2px 11px rgba(0, 0, 0, .21);
    font-family: iranyekanFaNum;
    &::-webkit-scrollbar-track {
         cursor: pointer;
         background-color: ${CONSTS.colors.grey[8]};
         border-radius: 40px;
      }

      &::-webkit-scrollbar {
         width: 5px;
         cursor: pointer;
         background-color: ${CONSTS.colors.grey[8]};
         border-radius: 40px;
      }

      &::-webkit-scrollbar-thumb {
         cursor: pointer;
         background-color: ${CONSTS.colors.grey[8]};
         border-radius: 40px;
      }
    > ._header{
        position: sticky;
        top: 0;
        z-index: 111;
        height: 51px;
        background-color: white;
        border-bottom: 1px solid ${CONSTS.colors.grey[8]};
        /* color: #fff; */
        display: flex;
        align-items: stretch;
        justify-content: space-between;
        padding:0 5px 0 0px;
        > ._title{
            display: flex;
            flex-direction: column;
            justify-content: center;
            text-align: left;
            padding: 0 20px;
            font-size: 14px;
            font-weight: bold;
        }
        > ._close{
            /* flex-basis: 51px; */
            padding-right: 10px;
            padding-left: 15px;
            padding-top: 5px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            cursor: pointer;
            position: relative;
            color: rgba(255, 255, 255, .8);
            transition: color .2s;
            color: ${CONSTS.colors.grey[4]};
            text-align: center;
            /* &:hover{
                color: rgba(255, 255, 255, 1);
            } */
        }
    }
    
    > ._body{
        padding: 10px 20px 25px 20px;
        /* max-height:90vh; */
        height:auto;
        /* overflow: auto; */
        position: relative;
        /* background:red; */
    }
    &.active{
        opacity: 1;
        transform:translate(-50%, -50%) scale(1);
        pointer-events: all;
    }
`;

export default StyledModal;