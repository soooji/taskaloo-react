import React from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { Flex, Icon } from "./";

function InputInfo({ className, title, icon = "info" }) {
  return (
    <Flex ai="center" jc="flex-start" className={className}>
      {title ? <Icon name={icon} /> : null}
      {title ? <div className="info-text">{title}</div> : null}
    </Flex>
  );
}
const StyledInputInfo = styled(InputInfo)`
  color: ${CONSTS.colors.error.normal};
  padding-right: 14px;
  margin-top: 4px;
  height: 20px;
  .info-text {
    font-family: ${CONSTS.fonts.normal};
    font-size: 14px;
    margin-right: 6px;
    font-weight: normal;
  }
`;

export default StyledInputInfo;
