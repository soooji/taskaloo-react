import React from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { Flex, Icon } from "./";

function Card({
  className,
  children,
  title,
  titleIcon,
  headerClassName,
  meta,
  containerStyle,
  underTitleMeta,
  responsiveHeader = false,
  containerOverflow = false,
  loading = false,
  ...props
}) {
  return (
    <div className={className} style={{ ...props.style }} {...props}>
      {title || meta ? (
        <Flex
          ai="center"
          jc="space-between"
          className={`card-header ${headerClassName ?? ""}`}
        >
          <Flex jc="flex-start" className="card-header__title">
            {titleIcon ? <Icon name={titleIcon} large={1} /> : null}
            <div className="card-header__title__text">{title}</div>
          </Flex>
          {meta ? <div className="card-header__meta">{meta}</div> : null}
        </Flex>
      ) : null}
      {underTitleMeta ?? null}
      <div className="card-container" style={{ ...containerStyle }}>
        {loading ? <div className="card-overlay"></div> : null}
        {children}
      </div>
    </div>
  );
}

const StyledCard = styled(Card)`
  font-family: "PeydaWeb";
  display: table;
  width: 100%;
  @media (max-width: ${CONSTS.responsive.md}px) {
    display: block;
  }
  .card-header {
    width: 100%;
    padding: 23px 30px;
    box-sizing: border-box;
    /* text-align: center; */
    @media (max-width: ${CONSTS.responsive.md}px) {
      padding: 23px 20px 13px 20px;
      ${(props) =>
        props.responsiveHeader
          ? `
        flex-direction: column;
      `
          : ``}
    }
    @media print {
      padding: 0px 13px 13px 20px !important;
    }
    &__title {
      &__text {
        ${(props) =>
          props.titleIcon ? "margin-right: 11px;" : "margin-right: 0px;"}
        font-family: "PeydaWeb";
        font-weight: 500;
        font-size: 16px;
        @media (max-width: ${CONSTS.responsive.md}px) {
          font-size: 13px;
        }
      }
    }
    &__meta {
      ${(props) =>
        props.responsiveHeader
          ? `
        margin-top: 15px;
      `
          : ``}
    }
  }
  .card-container {
    width: 100%;
    background: #ffffff;
    position: relative;
    ${(props) =>
      props.nostyle == 1
        ? `
        border: none;
        box-shadow: none;
    `
        : `
        border: 1px solid ${CONSTS.colors.grey[8]};
        box-shadow: 0 6px 32px 0 rgba(0, 0, 0, 0.03); 
    `}

    ${(props) =>
      props.containerOverflow
        ? `
      overflow-x: auto;
    `
        : ``}

    border-radius: ${CONSTS.borderRadius.outside};
    box-sizing: border-box;
    padding: ${(props) => (props.nopadding ? "0px" : "30px")};
    @media (max-width: ${CONSTS.responsive.md}px) {
      padding: ${(props) => (props.nopadding ? "0px" : "20px")};
    }
    > * {
      margin-top: 0;
    }
    @media print {
      border: none !important;
      padding: 0 !important;
    }
  }
  .card-overlay {
    z-index: 11;
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    background: rgba(255, 255, 255, 0.6);
  }
`;

export default StyledCard;
