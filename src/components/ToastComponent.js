import React from "react";
import styled from "styled-components";
import { DefaultToast } from "react-toast-notifications";
import CONSTS from "../utils/constants";

const ToastComponent = ({ children, ...props }) => (
  <DefaultToast {...props}>{children}</DefaultToast>
);

const StyledToastComponent = styled(ToastComponent)`
  font-family: ${CONSTS.fonts.normal};
  overflow: hidden !important;
  * {
    overflow: hidden !important;
  }
`;

export default StyledToastComponent;
