import React from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { Flex, Icon, Loading } from "./";

const Button = styled.button`
  padding: ${(props) => (props.icon != null ? "12px 12px" : "12px 24px")};
  background-color: ${CONSTS.colors.info.normal};
  border: 1px solid ${CONSTS.colors.info.normal};
  border-radius: ${CONSTS.borderRadius.normal};

  border: 1px solid
    ${(props) =>
      props.invert ? CONSTS.colors.info.light : CONSTS.colors.info.normal};
  background: ${(props) =>
    props.invert ? CONSTS.colors.info.light : CONSTS.colors.info.normal};

  color: ${(props) => (props.invert ? CONSTS.colors.info.dark : "white")};
  text-align: center;
  width: auto;
  transition: ease width 0.3s;
  font-size: 14px;
  position: relative;
  font-family: ${CONSTS.fonts.normal};
  font-weight: 500;
  cursor: pointer;
  outline: none;
  transition: background-color 0.15s ease-in, color 0.15s ease-in,
    transform 0.2s ease, box-shadow 0.15s ease;
  margin: ${(props) =>
    props.nomargin ? "0px" : props.block ? "15px 15px" : "15px 0 0 0"};
  transform: scale(1);

  ${(props) =>
    props.block
      ? `
        display: block;
        width: 100%;
    `
      : ""}

  ${(props) => `
    &:hover {
      @media only screen and (min-width: ${CONSTS.responsive.md}px) {
        background-color: ${
          props.invert ? CONSTS.colors.info.midLight : CONSTS.colors.info.dark
        };
      }
    }
  `}
  /* &:hover {
    @media only screen and (min-width: ${CONSTS.responsive.md}px) {
      background-color: ${(props) =>
    props.invert ? CONSTS.colors.info.midLight : CONSTS.colors.info.dark}};
    }
  } */
  &:focus {
    box-shadow: 0 0 0 ${(props) => (props.nooutline ? "0" : "3")}px
      ${(props) =>
        props.invert ? CONSTS.colors.info.light : CONSTS.colors.info.midLight};
    outline: none;
  }
  &:active {
    transform: scale(0.97);
  }
  ${(props) => `
        ${
          props.primary
            ? `
            border:1px solid ${
              props.invert
                ? CONSTS.colors.primary.light
                : CONSTS.colors.primary.normal
            };
            background: ${
              props.invert
                ? CONSTS.colors.primary.light
                : CONSTS.colors.primary.normal
            };
            ${
              props.invert
                ? `
              color: ${CONSTS.colors.primary.dark};
            `
                : ``
            }
            &:hover{
              background: ${
                props.invert
                  ? CONSTS.colors.primary.midLight
                  : CONSTS.colors.primary.dark
              }};
            }
            &:focus {
              box-shadow: 0 0 0 3px ${
                props.invert
                  ? CONSTS.colors.primary.light
                  : CONSTS.colors.primary.midLight
              };
            }
        `
            : ``
        }
        ${
          props.success
            ? `
            border:1px solid ${
              props.invert
                ? CONSTS.colors.success.light
                : CONSTS.colors.success.normal
            };
            background: ${
              props.invert
                ? CONSTS.colors.success.light
                : CONSTS.colors.success.normal
            };
            ${
              props.invert
                ? `
              color: ${CONSTS.colors.success.dark};
            `
                : ``
            }
            &:hover{
              background: ${
                props.invert
                  ? CONSTS.colors.success.midLight
                  : CONSTS.colors.success.dark
              }};
            }
            &:focus {
              box-shadow: 0 0 0 3px ${
                props.invert
                  ? CONSTS.colors.success.light
                  : CONSTS.colors.success.midLight
              };
            }
        `
            : ``
        }
        ${
          props.error
            ? `
            border:1px solid ${
              props.invert
                ? CONSTS.colors.error.light
                : CONSTS.colors.error.normal
            };
            background: ${
              props.invert
                ? CONSTS.colors.error.light
                : CONSTS.colors.error.normal
            };
            ${
              props.invert
                ? `
              color: ${CONSTS.colors.error.dark};
            `
                : ``
            }
            &:hover{
              background: ${
                props.invert
                  ? CONSTS.colors.error.midLight
                  : CONSTS.colors.error.dark
              }};
            }
            &:focus {
              box-shadow: 0 0 0 3px ${
                props.invert
                  ? CONSTS.colors.error.light
                  : CONSTS.colors.error.midLight
              };
            }
        `
            : ``
        }
        ${
          props.warning
            ? `
            border:1px solid ${
              props.invert
                ? CONSTS.colors.warning.light
                : CONSTS.colors.warning.normal
            };
            background: ${
              props.invert
                ? CONSTS.colors.warning.light
                : CONSTS.colors.warning.normal
            };
            ${
              props.invert
                ? `
              color: ${CONSTS.colors.warning.dark};
            `
                : ``
            }
            &:hover{
              background: ${
                props.invert
                  ? CONSTS.colors.warning.midLight
                  : CONSTS.colors.warning.dark
              }};
            }
            &:focus {
              box-shadow: 0 0 0 3px ${
                props.invert
                  ? CONSTS.colors.warning.light
                  : CONSTS.colors.warning.midLight
              };
            }
        `
            : ``
        }
        ${
          props.info
            ? `
            border:1px solid ${
              props.invert ? CONSTS.colors.grey[9] : CONSTS.colors.grey[5]
            };
            background: ${
              props.invert ? CONSTS.colors.grey[9] : CONSTS.colors.grey[5]
            };
            ${
              props.invert
                ? `
              color: ${CONSTS.colors.grey[4]};
            `
                : ``
            }
            &:hover{
              background: ${
                props.invert ? CONSTS.colors.grey[8] : CONSTS.colors.grey[4]
              };
            }
            &:focus {
              box-shadow: 0 0 0 3px ${
                props.invert ? CONSTS.colors.grey[7] : CONSTS.colors.grey[6]
              };
            }
        `
            : ``
        }
        ${
          props.outline && !props.loading
            ? `
              border:1px solid ${CONSTS.colors.info.normal};
              color: ${CONSTS.colors.info.normal};
              background: ${CONSTS.colors.info.light};
              &:hover{
                  background: ${CONSTS.colors.info.normal};
                  color: #fff;
              }
            
            ${
              props.primary
                ? `
                border:1px solid ${CONSTS.colors.primary.normal};
                color: ${CONSTS.colors.primary.normal};
                background: ${CONSTS.colors.primary.light};
                &:hover{
                    background: ${CONSTS.colors.primary.normal};
                    color: #fff;
                }
            `
                : ``
            }

            ${
              props.success
                ? `
                border:1px solid ${CONSTS.colors.success.normal};
                color: ${CONSTS.colors.success.normal};
                background: ${CONSTS.colors.success.light};
                &:hover{
                    background: ${CONSTS.colors.success.normal};
                    color: #fff;
                }
            `
                : ``
            }
            ${
              props.error
                ? `
                border:1px solid ${CONSTS.colors.error.normal};
                color: ${CONSTS.colors.error.normal};
                background: ${CONSTS.colors.error.light};
                &:hover{
                    background: ${CONSTS.colors.error.normal};
                    color: #fff;
                }
            `
                : ``
            }
            ${
              props.warning
                ? `
                border:1px solid ${CONSTS.colors.warning.normal};
                color: ${CONSTS.colors.warning.normal};
                background: ${CONSTS.colors.warning.light};
                &:hover{
                    background: ${CONSTS.colors.warning.normal};
                    color: #fff;
                }
            `
                : ``
            }
            ${
              props.info
                ? `
                border:1px solid ${CONSTS.colors.grey[5]};
                color: ${CONSTS.colors.grey[4]};
                background: ${CONSTS.colors.grey[9]};
                &:hover{
                    background: ${CONSTS.colors.grey[5]};
                    color: #fff;
                }
            `
                : ``
            }
        `
            : ``
        }
        ${
          props.link
            ? `
            border: none;
            background: transparent;
            padding: 5px 10px;
            color: ${CONSTS.colors.info.normal};
            &:hover{
                color: ${CONSTS.colors.info.dark};
                background: transparent;
            }
        `
            : ``
        }
        ${
          props.small
            ? `
            font-size: 12px;
        `
            : `
        `
        }
      ${
        props.disabled
          ? `
              // background-color: ${CONSTS.colors.grey[8]};
              opacity: 0.4;
              // &:hover {
              //   opacity: 1;
              //   background-color: ${CONSTS.colors.grey[8]};
              // }
              // &:active {
              //   transform: scale(1);
              // }
          `
          : ``
      }
    `}
`;
const Btn = (props) => (
  <Button
    {...props}
    disabled={props.isLoading || props.disabled}
    className={props.className + " noselect"}
    style={{
      ...props.style,
      position: "relative",
      cursor: props.disabled || props.isLoading ? "default" : "pointer",
    }}
  >
    <Flex ai="center" jc="center">
      {props.icon != null ? (
        <Icon
          name={props.icon}
          small={props.small ? 1 : 0}
          medium={!props.small ? 1 : 0}
          style={{
            marginLeft: props.small ? "6px" : "7px",
            opacity: props.loading ? 0.1 : 1,
            ...props.iconStyle,
          }}
        />
      ) : null}
      <div style={{ opacity: props.loading ? 0.1 : 1 }}>{props.children}</div>
    </Flex>
    {props.loading ? (
      <Flex
        ai="center"
        jc="center"
        style={{
          width: "100%",
          height: "100%",
          position: "absolute",
          zIndex: 11,
          top: 0,
          right: 0,
        }}
      >
        <Loading
          invert={!props.invert}
          small={1}
          primary={props.primary}
          info={props.info}
          success={props.success}
          error={props.error}
          warning={props.warning}
        />
      </Flex>
    ) : null}
  </Button>
);
export default Btn;
