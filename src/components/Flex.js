import styled from "styled-components";
const Flex = styled.div`
  ${({ fd, fw, jc, ai, ac }) => `
   display: flex;
   flex-direction: ${fd ?? "row"};
   flex-wrap: ${fw ?? "nowrap"};
   justify-content: ${jc ?? "flex-start"};
   align-items: ${ai ?? "stretch"};
   align-content: ${ac ?? "stretch"};
`}
`;
export default Flex;
