import React, { useState } from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { Button, Flex } from "./";
import Persify from "persify";

const stepItems = [
  {
    index: 1,
    title: "اطلاعات کلی گزارش",
  },
  {
    index: 2,
    title: "اطلاعات دام",
  },
  {
    index: 3,
    title: "خوراک‌ها",
  },
  {
    index: 4,
    title: "مقادیر خوراک",
  },
  {
    index: 5,
    title: "گزارش تحلیلی",
  },
];

function Steps({
  className,
  activeStep,
  setActiveStep,
  goPrev,
  goNext,
  ...props
}) {
  const changeStep = (index) => {
    if (index <= activeStep) {
      setActiveStep(index);
    }
  };
  return (
    <Flex jc="flex-start" ai="center" className={className} {...props}>
      <Button
        onClick={goPrev}
        className="prevnext-button prev-btn"
        invert={1}
        info={1}
        icon="forward"
      ></Button>
      {/* TODO: On responsive, check situations for going forward and backward in steps */}
      {stepItems.map((v, k) => (
        <StepItem
          key={k}
          index={v.index}
          onClick={() => changeStep(v.index)}
          activeIndex={activeStep}
          title={v.title}
        />
      ))}
      <Button
        onClick={goNext}
        className="prevnext-button next-btn"
        invert={1}
        info={1}
        icon="back"
      ></Button>
    </Flex>
  );
}

const StyleSteps = styled(Steps)`
  ${(props) =>
    `border-bottom: ${props.noBorder ? "0px" : "1px"} solid ${
      CONSTS.colors.grey[8]
    };`}
  .prevnext-button {
    display: none;
    height: 43px;
    width: 43px;
    padding: 0 !important;
    margin-top: 0;
    span {
      margin-left: 0px !important;
    }
    &.next-btn {
      margin-left: 7px;
    }
    &.prev-btn {
      margin-right: 7px;
    }
  }
  @media only screen and (max-width: ${CONSTS.responsive.lg + 200}px) {
    justify-content: space-between;
    .prevnext-button {
      display: inline-block;
    }
  }
`;

function StepItemCmp({
  className,
  index,
  title,
  activeIndex,
  noArrow,
  ...props
}) {
  return (
    <Flex
      jc="flex-start"
      ai="center"
      className={`${className} noselect`}
      {...props}
    >
      <Flex ai="center" jc="center" className="number">
        {Persify("" + index)}
      </Flex>
      <div className="title">{title}</div>
      <div className="back-icon"></div>
    </Flex>
  );
}

const StepItem = styled(StepItemCmp)`
  font-family: ${CONSTS.fonts.normal};
  margin-right: 18px;
  height: 55px;
  @media only screen and (max-width: ${CONSTS.responsive.lg + 200}px) {
    margin-right: 0;
  }
  ${(props) => `
        ${
          props.activeIndex > props.index
            ? `color: ${CONSTS.colors.grey[4]};`
            : ``
        }
      ${
        props.activeIndex == props.index
          ? `color: ${CONSTS.colors.primary.normal};`
          : ``
      }
      ${
        props.activeIndex < props.index
          ? `color: ${CONSTS.colors.grey[6]};`
          : ``
      }
      ${props.activeIndex >= props.index ? `cursor: pointer;` : ``}

      ${
        props.activeIndex != props.index
          ? `
          @media only screen and (max-width: ${CONSTS.responsive.lg + 200}px) {
            display: none;
          }
        `
          : ``
      }
  `}
  transition: ease color .2s;
  .number {
    background: ${CONSTS.colors.grey[9]};
    transition: ease background 0.2s;
    font-size: 16px;
    text-align: center;
    width: 27px;
    height: 27px;
    border-radius: 20px;
    ${(props) =>
      props.activeIndex == props.index
        ? `background: ${CONSTS.colors.primary.light};`
        : ``}
  }
  .title {
    font-size: 16px;
    text-align: right;
    margin-right: 11px;
  }
  .back-icon {
    border: solid ${CONSTS.colors.grey[8]};
    border-width: 0 1px 1px 0;
    display: inline-block;
    transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
    height: 38px;
    width: 38px;
    margin-right: -10px;
    margin-left: 5px;
  }
  &:last-child {
    .back-icon {
      display: none;
    }
  }
  @media only screen and (max-width: ${CONSTS.responsive.lg + 200}px) {
    .back-icon {
      display: none;
    }
  }
`;

export default StyleSteps;
