import React from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { Flex, Icon } from "./";
import { Link } from "react-router-dom";

function NewItemCard({ className, title, link, ...props }) {
  return link ? (
    <Link to={link} className={className + " noselect"} {...props}>
      <Flex fd="column" ai="center" jc="flex-start">
        <Icon className={"group-icon"} name="plus" large={1} size="50px" />
        <div className={"group-name"}>{title ?? "مورد جدید"}</div>
      </Flex>
    </Link>
  ) : (
    <div className={className + " noselect"} {...props}>
      <Flex fd="column" ai="center" jc="flex-start">
        <Icon className={"group-icon"} name="plus" large={1} size="50px" />
        <div className={"group-name"}>{title ?? "مورد جدید"}</div>
      </Flex>
    </div>
  );
}
const StyledNewItemCard = styled(NewItemCard)`
  border: 1px solid ${CONSTS.colors.success.midLight};
  background: ${CONSTS.colors.success.light};
  box-shadow: 0 6px 32px 0 rgba(0, 0, 0, 0.03);
  border-radius: 15px;
  min-height: 156px;
  margin-bottom: 20px;
  margin-right: 20px;
  cursor: pointer;
  transition: ease background 0.2s, ease transform 0.2s, ease border 0.2s;
  .group-icon {
    margin-top: 35px;
    color: ${CONSTS.colors.success.normal};
  }
  .group-icon-image {
    margin-top: 35px;
    width: auto;
    height: 50px;
    object-fit: contain;
    object-position: center;
  }
  .group-name {
    margin-top: 23px;
    font-family: ${CONSTS.fonts.normal};
    font-weight: 500;
    font-size: 16px;
    color: ${CONSTS.colors.success.normal};
    text-align: center;
  }
  &:hover {
    /* background: ${CONSTS.colors.success.midLight}; */
    border-color: ${CONSTS.colors.success.normal};
    transform: scale(1.05);
  }
  &:active {
    transform: scale(0.98);
  }
`;

export default StyledNewItemCard;
