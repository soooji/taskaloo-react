import React, { useEffect, useState } from "react";
import styled from "styled-components";
// import { Chart } from "react-charts";
import Chart from "react-apexcharts";
import { Card, Button, Flex, ButtonGroup } from "../components";
import CONSTS from "../utils/constants";
import { Link } from "react-router-dom";
// import StatsReq from "api/statistics";
import { getCachedUser } from "../utils/functions";
import { random } from "lodash";
import moment from "moment";
// import persify from "persify";


function ChartPartCmp({ className,stats,title,...props }) {
    //-- STATES --//
    const [activeChoice, setActiveChoice] = useState("months"); //week or months
    const [chartWidth, setChartWidth] = useState(100);

    const [xAxisData, setXAxisData] = useState([]);
    const [yAxisData, setYAxisData] = useState([]); //2D array: index is activeChart
    //-- HOOKS --//
    useEffect(() => {
        let d = document.getElementById("ChartEl").clientWidth;
        setChartWidth(d);
    }, []);

    useEffect(() => {
        if (stats) {
            let xdata = [];
            let ydata = [];
            if (activeChoice === "weeks") {
                stats.weak.forEach((v) => {
                    xdata.push(moment(v?.date).format("YYYY-MM-DD"))
                    ydata.push(v?.count)
                });
            } else {
                stats.month.forEach((v) => {
                    xdata.push(moment(v?.date).format("YYYY-MM-DD"))
                    ydata.push(v?.count)
                });
            }
            setXAxisData(xdata);
            setYAxisData(ydata);
            console.log(xdata);
            console.log(ydata);
        }
    }, [stats, activeChoice]);

    let ChartOptions = {
        options: {
            chart: {
                id: "basic-bar",
                zoom: {
                    type: "x",
                    enabled: false,
                    autoScaleYaxis: false,
                },
                toolbar: {
                    show: false,
                },
            },
            xaxis: {
                categories: xAxisData,
                labels: {
                    formatter: function (val) {
                        return val;
                    },
                },
                tooltip: {
                    enabled: false,
                },
            },
            fill: {
                type: "gradient",
                gradient: {
                    shadeIntensity: 1,
                    inverseColors: false,
                    opacityFrom: 0.5,
                    opacityTo: 0,
                    stops: [0, 90, 100],
                },
            },
            yaxis: {
                forceNiceScale: false,
                decimalsInFloat: 0,
                floating: false,
                labels: {
                    formatter: function (val) {
                        return val.toFixed(0);
                    },

                    style: {
                        fontFamily: "Vazir FD",
                    },
                },
            },
            markers: {
                size: 4,
                colors: ["#f36d21"],
                strokeColors: "#fff",
                strokeWidth: 2,
                hover: {
                    size: 7,
                },
            },
            stroke: {
                width: 7,
                curve: "smooth",
            },
            tooltip: {
                style: {
                    fontFamily: "Vazir FD",
                },
                x: {
                    show: true,
                },
                y: {},
            },
            dataLabels: {
                enabled: false,
            },
            legend: {
                show: false,
            },
        },
        series: [
            {
                name:
                    activeChoice === "weeks" ?
                     "گزارش‌گیری هفتگی" : "گزارش‌گیری ماهانه",
                data: yAxisData,
            },
        ],
    };

    //-- OTHER FUNCTIONS --//

    return (
        <Card
            title={title}
            id="ChartEl"
            meta={
                <ButtonGroup>
                    <Button
                        onClick={() => setActiveChoice("weeks")}
                        invert={activeChoice != "weeks" ? 1 : 0}
                        small={1}
                        nomargin={1}
                    >
                        هفتگی
                    </Button>
                    <Button
                        onClick={() => setActiveChoice("months")}
                        invert={activeChoice != "months" ? 1 : 0}
                        small={1}
                        nomargin={1}
                    >
                        ماهانه
                    </Button>
                </ButtonGroup>
            }
            className={className}
            responsiveHeader={1}
        >
            <div className="chart-container">
                {/* <Chart tooltip data={data} axes={axes} /> */}

                <Chart
                    key={chartWidth}
                    options={ChartOptions.options}
                    series={ChartOptions.series}
                    type="area"
                    height={450}
                />
            </div>
            <br />
        </Card>
    );
}

const ChartPart = styled(ChartPartCmp)`
  flex: 2;
  margin-bottom: 30px;
  .chart-container {
    /* background:red;   */
    margin-right: 30px;
    margin-left: 30px;
    height: 390px;
    margin-top: 10px;
  }
`;



export default ChartPart;
