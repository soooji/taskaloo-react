import React from 'react';
import styled from 'styled-components';

const Icon = ({className, name, ...props}) => (
    <span className={`${className} icon-${name}`} {...props}></span>
)
const StyledIcon = styled(Icon)`
    font-family: 'jiredaam';
    ${(props) => `
        ${props.mini ? `font-size: 12px;` : ``}
        ${props.small ? `font-size: 14px;` : ``}
        ${props.medium ? `font-size: 16px;` : ``}
        ${props.large ? `font-size: 19px;` : ``}
        ${props.size ? `font-size: ${props.size};` : ``}
    `}
`
export default StyledIcon;