import React, { useState, useEffect } from "react";
import styled from "styled-components";
import {
  Card,
  Button,
  Flex,
  GroupCard,
  AnimationLoading,
  NewItemCard,
  Icon,
} from "./";
import { Modal } from "react-responsive-modal";
import { getCachedUser } from "../utils/functions";
import CONSTS from "../utils/constants";
import EditTask from "../containers/projects/EditTask";

function MiniTask({ className, name, users, tags, id, refresh }) {
  const [isMenuOpen, setisMenuOpen] = useState(false);
  return (
    <div className={className}>
      <Modal
        open={isMenuOpen}
        onClose={() => setisMenuOpen(false)}
        center
        classNames={{
          modal: "normal-modal__container",
          overlay: "normal-modal__overlay",
          closeButton: "normal-modal__close-button",
        }}
      >
        <Flex className="modal-title-bar">
          <div className="modal-title-bar__text">{name}</div>
        </Flex>
        <EditTask id={id} refresh={refresh} />
      </Modal>
      <Card
        containerStyle={{
          padding: "5px 5px",
          backgroundColor: CONSTS.colors.primary.light,
          borderRight: `5px solid ${CONSTS.colors.primary.normal}`,
        }}
        className="card-style"
        onClick={() => setisMenuOpen(true)}
      >
        <div className="container">
          <div>
            <div className="name-style">{name}</div>
            <div>
              <div className="members">
                <Icon className="icon-style" name="account" />
                {users
                  ? users.map((u, o) => (
                      <div className="each-user">
                        {u.first_name +
                          " " +
                          u.last_name +
                          `${o !== users.length - 1 ? "،" : ""}`}
                      </div>
                    ))
                  : null}
              </div>
              <div className="tags">
                <Icon className="icon-style2" name="combo_chart" />
                {tags
                  ? tags.map((t, p) => (
                      <div className="each-tag">
                        {t.name + `${p !== tags.length - 1 ? "،" : ""}`}
                      </div>
                    ))
                  : null}
              </div>
            </div>
          </div>
          {/* <Flex>
                        <Icon className="trash-style" name="trash_can" onClick={onDelete}/>
                        <Icon className="back-style" name="back" onClick={()=>setisMenuOpen(true)}/>
                    </Flex> */}
        </div>
      </Card>
    </div>
  );
}
const StyledMiniTask = styled(MiniTask)`
  font-family: "PeydaWeb";
  .card-style {
    margin-bottom: 10px;
    padding: 0;
    /* width:100%;  */
    cursor: pointer;
  }

  .container {
    /* background:red; */
    padding: 10px;
  }
  .each-user {
    display: inline-block;
  }
  .each-tag {
    display: inline-block;
  }
  .name-style {
    margin-bottom: 10px;
    font-weight: 600;
  }
  .members {
    /* background:${CONSTS.colors.primary.light}; */
    border-radius: 15px;
    padding: 1px;
    color: ${CONSTS.colors.primary.normal};
    font-size: 13px;
    padding-left: 7px;
    margin-left: 10px;
    display: inline-block;
  }
  .icon-style {
    /* background:${CONSTS.colors.primary.midLight}; */
    border-radius: 15px;
    padding: 2px;
    margin-left: 2px;
  }
  .tags {
    /* background:${CONSTS.colors.error.light}; */
    border-radius: 15px;
    padding: 1px;
    color: ${CONSTS.colors.error.normal};
    font-size: 13px;
    padding-left: 7px;
  }
  .icon-style2 {
    /* background:${CONSTS.colors.error.midLight}; */
    border-radius: 15px;
    padding: 2px;
    margin-left: 2px;
  }
  .trash-style {
    background: ${CONSTS.colors.error.light};
    padding: 7px;
    font-size: 18px;
    color: ${CONSTS.colors.error.normal};
    border-radius: 7px;
    margin-left: 8px;
    cursor: pointer;
  }
  .back-style {
    background: ${CONSTS.colors.primary.light};
    padding: 7px;
    font-size: 18px;
    color: ${CONSTS.colors.primary.normal};
    border-radius: 7px;
    cursor: pointer;
  }
`;
export default StyledMiniTask;
