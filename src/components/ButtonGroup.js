import React from 'react';
import styled from "styled-components";
import {Flex} from './'

function InputInfo({ className, children, ...props}) {
  return (
    <Flex ai="flex-start" jc="flex-start" className={className} {...props}>
      {children}
    </Flex>
  );
}
const StyledInputInfo = styled(InputInfo)`
  > * {
     margin-left: 15px;
  }
  *:last-child {
    margin-left: 0;
  }
`;

export default StyledInputInfo;
