import React, {  } from "react";
import { useFormikContext } from "formik";

const AutoSetValue = ({ setValues }) => {
  const { values } = useFormikContext();
  React.useEffect(() => {
    setValues(values);
  }, [values]);
  return null;
};

export default AutoSetValue;
