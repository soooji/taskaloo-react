import React, { useState } from "react";
import styled from "styled-components";
import CONSTS from '../utils/constants';
import {Icon, Button, Modal} from './'
import { Label } from "./Typography";
import DatePicker from "react-modern-calendar-datepicker";
// import TimePicker from 'rc-time-picker';

// import moment from 'moment';
// import DateTimePicker from 'react-datetime-picker';
// import TimePicker from 'react-time-picker';

// const format = 'h:mm a';
// const now = moment().hour(0).minute(0);

const commonStyles = (props, selector) => `
    position: relative;
    flex:1;
    display:block;
    // min-width:0;
    // margin-bottom:15px;
    // &:last-child {
    //     margin-left: 20px;
    // }
    // &:first-child {
    //     margin-right: auto;
    // }
    ${
      props.small &&
      `
        margin-left: 4px;
        margin-right: 4px;
    `
    }
     ._icon{
        position: absolute;
        left: ${props.small ? "5" : "15"}px;
        top: 50%;
        transform: translateY(-50%);
        color: ${CONSTS.colors.info.normal};
        font-size: 14px;
    }
     ${selector}{
        background: transparent;
        ${props.icon ? `padding-left: ${props.small ? "25" : "45"}px;` : ""}
        border: 1px solid ${CONSTS.colors.grey[8]};
        font-family: ${CONSTS.fonts.normal};
        font-weight: 500;
        color: black;
        border-radius: ${CONSTS.borderRadius.normal};
        font-size: 14px;
        box-sizing: border-box;
      //   margin-bottom:20px;
      // margin-bottom: 6px;
        transition: ease background .2s,ease border-color .2s,ease box-shadow .2s;
        box-shadow: 0 0 0 0px ${CONSTS.colors.info.midLight};
        ${
          props.small
            ? `
                padding: 7px 10px;
            
            `
            : `
                padding: 11px 15px;
                width: 100%;
            `
        }

        :focus{
            box-shadow: 0 0 0 3px ${CONSTS.colors.info.midLight};
            border-color: ${CONSTS.colors.info.normal};
            // background: ${CONSTS.colors.info.light};
            outline: none;
            color: #333;
        }
        ::placeholder{
            color: ${CONSTS.colors.grey[5]};
            font-size: 12px;
            // font-weight: 500;
            font-family: ${CONSTS.fonts.normal};
            direction: rtl;
        }
        // :disabled{
          // color: ${CONSTS.colors.grey[5]};
        // }
    }
    ${
      props.error
        ? `
        > ${selector}, > ._icon{
            border-color: ${CONSTS.colors.error.normal} !important;
            :focus{
               box-shadow: 0 0 0 3px ${CONSTS.colors.error.midLight};
               border-color: ${CONSTS.colors.error.normal};
           }
        }
    `
        : ``
    }
`;

const Input = (props) => (
  <div className={props.className}>
    {props.icon && <Icon className="_icon" icon={props.icon} />}
    <input {...props} className="" />
  </div>
);
const StyledInput = styled(Input)`
  ${(props) => commonStyles(props, "input")}
  input:invalid:focus {
    border: 1px solid ${CONSTS.colors.error.light};
  }
`;

const HiddenInput = (props) => <input {...props} className="" />;
StyledInput.Hidden = HiddenInput;

const Textarea = (props) => (
  <div className={props.className}>
    <textarea {...props} className="">
      {props.value}
    </textarea>
  </div>
);
const StyledTextarea = styled(Textarea)`
  ${(props) => commonStyles(props, "textarea")}
  > textarea {
    resize: vertical;
    min-height: 100px;
  }
`;
StyledInput.Textarea = StyledTextarea;

const Select = (props) => (
  <div className={props.className}>
    <select {...props} className="">
      <option selected>
        {props.placeholder ? props.placeholder : ""}
      </option>
      {props.options &&
        props.options.map((opt, i) => (
          <option key={i} value={opt.value}>
            {opt.label}
          </option>
        ))}
    </select>
  </div>
);
const StyledSelect = styled(Select)`
    ${(props) => commonStyles(props, "select")}
    height: ${({ small }) => (small ? "37px" : "initial")};
    select {
        font-size: 13px;
        padding: 0em 1em 0em 3.5em;
        height: 100%;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-appearance: none;
        -moz-appearance: none;
        background-image:
            linear-gradient(45deg, transparent 50%, gray 50%),
            linear-gradient(135deg, gray 50%, transparent 50%);
        background-position:
        calc(10px) calc(50%),calc(15px) calc(50%);
        background-size:
            5px 5px,
            5px 5px,
            1px 1.5em;
        background-repeat: no-repeat;
    }
`;
StyledInput.Select = StyledSelect;

const RelationalSelect = (props) => {
  const [modalActive, setModalActive] = useState(false);
  const toggleModal = () => {
    setModalActive(!modalActive);
  };
  const handleSubmit = (res) => {
    alert("done");
    toggleModal();
  };
  const entity = {
    create: {
      api_form_url: "/niloufar/countries/form",
      api_url: "/av1/countries",
      post_obj_name: "country",
      title: "New Country",
      submitText: "Add new Country",
    },
  };
  return (
    <div className={props.className}>
      <Modal
        style={{ width: 700 }}
        onToggle={toggleModal}
        active={modalActive}
        title="Add New"
      >
        {/* <DynamicCreateInner onSubmit={handleSubmit} entity={entity}/> */}
      </Modal>
      <select {...props} className="">
        <option disabled selected hidden>
          {props.placeholder ? props.placeholder : "Select"}
        </option>
        {props.options &&
          props.options.map((opt, i) => (
            <option key={i} value={opt.value}>
              {opt.label}
            </option>
          ))}
      </select>
      <div style={{ marginLeft: "auto" }}>
        <Button type="button" link onClick={toggleModal}>
          +&nbsp;New
        </Button>
      </div>
    </div>
  );
};

const StyledRelationalSelect = styled(RelationalSelect)`
  ${(props) => commonStyles(props, "select")}
  display: flex;
  & > select {
    /* width: 80%; */
  }
  & > button {
  }
`;
StyledInput.RelationalSelect = StyledRelationalSelect;

const Radio = (props) => {
  const def = props.value
    ? props.value
    : props.defaultValue
    ? props.defaultValue
    : "";
  return (
    <div
      className={props.className}
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
      }}
    >
      {props.options &&
        props.options.map((opt, i) => (
          <Label
            key={i}
            className="_item"
            style={{
              display: "inline-block",
              width: `${100 / props.options.length - 1}%`,
              marginLeft: 0,
              marginRight: 0,
            }}
          >
            <input
              {...props}
              value={opt.value}
              className=""
              type="radio"
              style={{width: 0}}
              checked={opt.value == def}
            />
            <div className="checkbox-title">
               {opt.name}
            </div>
          </Label>
        ))}
    </div>
  );
};
const StyledRadio = styled(Radio)`
  ${(props) => commonStyles(props, "label")}
  input[type="radio"] {
    -webkit-appearance: none;
  }
  .checkbox-title {
     display: inline-block;
     margin-right: 7px;
  }
  > label {
    padding: 11px 14px;
    cursor: pointer;
    margin: 0;
    input {
      margin-right: 10px;
      position: relative;
      width: 0;
      height: 0;
      &::after {
        top: -10px;
        position: absolute;
        content: "";
        right: -10px;
        /* bottom: 0; */
        border-radius: 2px;
        width: 10px;
        height: 10px;
        background-color: transparent;
        border: 1px solid #DEE2E6;
        transition: background 0.25s, border-color 0.25s;
      }
      &:checked {
        &::after {
          background-color: ${CONSTS.colors.info.normal};
          border-color: ${CONSTS.colors.info.normal};
        }
      }
    }
  }
`;
StyledInput.Radio = StyledRadio;

const Checkbox = (props) => {
  // const def = props.value ? props.value : (props.defaultValue ? props.defaultValue : "");
  return (
    <div className={props.className}>
      <Label className="_item" style={!props.disableTopLabel ? {marginTop:0} : {}}>
        <input
          {...props}
          style={{width:0}}
          type="checkbox"
          checked={props.value}
          value={props.value}
        />
        <div className="checkbox-title">
         {props.label}
        </div>
      </Label>
    </div>
  );
};
const StyledCheckbox = styled(Checkbox)`
  ${(props) => commonStyles(props, "label")}
  /* margin-top: 10px; */
  /* margin-bottom: 10px; */
  .checkbox-title {
     margin-left: auto;
     margin-right: 7px;
  }
  > label {
   margin-right: 0;
    /* padding: 10px 15px; */
    cursor: pointer;
    display: flex;
    justify-content: flex-start;
    flex-direction: row;
    align-items: center;
    input {
      margin-right: 10px;
      position: relative;
      width: 0;
      flex: initial;
      height: 0;
      &::after {
        position: absolute;
        content: "";
        right: -11px;
        bottom: 0;
        top: -6px;
        border-radius: 2px;
        width: 10px;
        height: 10px;
        background-color: transparent;
        border: 1px solid #999;
        transition: background 0.25s, border-color 0.25s;
      }
      &:checked {
        &::after {
          background-color: ${CONSTS.colors.info.normal};
          border-color: ${CONSTS.colors.info.normal};
        }
      }
    }
  }
`;
StyledInput.Checkbox = StyledCheckbox;

const SingleInput = (props) => <input {...props} />;
StyledInput.SingleInput = styled(SingleInput)`
  ${(props) => commonStyles(props, "input")}
`;

// const Input = (props) => (
//     <div className={props.className}>
//         {props.icon && <Icon className="_icon" icon={props.icon} />}
//         <input {...props} className=""/>
//     </div>
// )
// const StyledInput = styled(Input)`
//     ${props => commonStyles(props, "input")}
// `;

const DateTime = (props) => {
  return (
    <div className={props.className}>
      {props.icon && <Icon className="_icon" icon={props.icon} />}
      <DatePicker
      style={{zIndex: 1111111,width: '100%'}}
        renderInput={(a) => console.log(a)}
        value={props.value && props.value}
        onChange={props.onChange && props.onChange}
        inputPlaceholder={props.placeholder}
        shouldHighlightWeekends
        locale="fa"
        for={props.for && props.for}
      />
    </div>
  );



  // return (
  //     <div className={props.className}>
  //         <DatePicker
  //             renderInput={(a)=>console.log(a)}
  //             value={props.value && props.value}
  //             onChange={props.onChange && props.onChange}
  //             inputPlaceholder={props.placeholder}
  //             shouldHighlightWeekends
  //             locale="fa"
  //         />
  //     </div>
  // )
};
const StyledDateTime = styled(DateTime)`
    ${(props) => commonStyles(props, "input")}
    input {
        text-align: right;
    }
    /* .react-datetime-picker__wrapper{
        background: transparent;
        padding: 15px 25px;
        border: 1px solid rgba(100, 100, 100, .2);
        color: #666;
        border-radius: ${CONSTS.borderRadius.normal};
        text-align: left;
        font-size: 15px;
        box-sizing: border-box;
        margin-top:5px;
        margin-bottom:5px;
        :focus{
            outline: none;
            border-color: rgba(100, 100, 100, .4);
            color: #333;
        }
        ::placeholder{
            color:#999;
        }
    } */
`;
StyledInput.DateTime = StyledDateTime;


// const Time = (props) => {
//   return (
//     <div className={props.className}>
//       {props.icon && <Icon className="_icon" icon={props.icon} />}
//       <TimePicker
//         // style={{zIndex: 1111111,width: '100%'}}
//         renderInput={(a) => console.log(a)}
//         defaultValue={props.value && props.value}
//         onChange={props.onChange && props.onChange}
//         inputPlaceholder={props.placeholder}
//         showSecond={false}
//         // defaultValue={moment()}
//         // defaultValue={now}
//         allowEmpty={false}
//       />
//     </div>
//   );
// };

// const StyledTime = styled(Time)`
//     ${(props) => commonStyles(props, "input")}
//     input {
//         text-align: right;
//         height: 100%;
//     }
// `;
// StyledInput.Time = StyledTime;

export default StyledInput;