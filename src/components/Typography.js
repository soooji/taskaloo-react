import React from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { Link as RLink } from "react-router-dom";
import ReactTooltip from "react-tooltip";
import { Icon } from "./";

const Heading = (props) => {
  var h = props.h ? props.h : 1;
  return (
    <>
      {h === 1 && <h1 {...props}>{props.children}</h1>}
      {h === 2 && <h2 {...props}>{props.children}</h2>}
      {h === 3 && <h3 {...props}>{props.children}</h3>}
      {h === 4 && <h4 {...props}>{props.children}</h4>}
      {h === 5 && <h5 {...props}>{props.children}</h5>}
      {h === 6 && <h6 {...props}>{props.children}</h6>}
    </>
  );
};
export const Title = styled(Heading)`
  color: rgba(77, 76, 101, 1);
  ${(props) => props.primary && `color: ${CONSTS.colors.info.normal};`}
  ${(props) => props.light && `color: #fff;`}
    ${(props) => props.center && `text-align: center;`}
    ${(props) => props.left && `text-align: left;`}
    ${(props) => props.right && `text-align: right;`}
    ${(props) => props.m0 && `margin: 0`}
`;

const Anchor = ({
  className,
  to = null,
  href = null,
  onClick,
  children,
  style,
}) => {
  const props = {
    className,
    style,
    onClick,
  };
  return (
    <>
      {to && (
        <RLink {...props} to={to}>
          {children}
        </RLink>
      )}
      {href && (
        <a {...props} href={href}>
          {children}
        </a>
      )}
    </>
  );
};
export const Link = styled(Anchor)`
  font-size: 14px;
  font-weight: 500;
  transition: color 0.2s;
  ${(props) =>
    props.light &&
    `
                font-weight: lighter;
            `}
  ${(props) =>
    props.small &&
    `
                font-size: 11px;
            `}
    ${(props) =>
    props.large &&
    `
                font-size: 16px;
            `}
    ${(props) =>
    props.underline
      ? ``
      : `
            text-decoration: none;
        `}
    color: #333;
  &:hover {
    color: #000;
  }
  ${(props) =>
    props.primary &&
    `
            color: ${CONSTS.colors.info.normal};
            &:hover{
                color: ${CONSTS.colors.info.dark};
            }
        `}
  ${(props) =>
    props.muted &&
    `
            color: rgba(211, 212, 218, 1);
            &:hover{
                color: #999;
            }
        `}
    ${(props) =>
    props.white &&
    `
            color: #eee;
            &:hover{
                color: #fff;
            }
        `}
`;

export const Text = styled.p`
  margin: 0;
  color: #333;
  ${(props) => {
    return `
            ${props.muted ? `color: #ccc;` : ""}
            ${
              props.small
                ? `
                font-size: 12px;
                line-height: 15px;
            `
                : `
                font-size: 14px;
                line-height: 20px;
            `
            }
            ${
              props.justify
                ? `
                text-align: justify;
            `
                : ``
            }
            ${
              props.left
                ? `
                text-align: left;
            `
                : ``
            }
            ${
              props.right
                ? `
                text-align: right;
            `
                : ``
            }
            ${
              props.light
                ? `
                font-weight: lighter;
            `
                : ``
            }
        `;
  }}
`;

export const Span = styled.span`
  ${(props) =>
    props.error
      ? `
        color: ${CONSTS.colors.error.normal}
    `
      : ``}
  ${(props) =>
    props.primary
      ? `
        color: ${CONSTS.colors.info.normal}
    `
      : ``}
`;

const StyledLabel = styled.label`
  padding-right: 14px;
  padding-left: 14px;
  margin-top: ${(props) => (props.nomargin ? "0px" : "15px")};
  padding-bottom: 5px;
  display: inline-block;
  font-size: 14px;
  font-family: ${CONSTS.fonts.normal};
  color: #000000;
  font-weight: 300;
  width: ${(props) => (props.noFill ? "auto" : "100%")};
  ${(props) => (props.noFill ? "max-width: 70%;" : "")};
  box-sizing: border-box;
  display: table;
  .input-toolip {
    max-width: 250px;
    letter-spacing: 0;
    padding-top: 15px;
    padding-bottom: 15px;
    border-radius: ${CONSTS.borderRadius.normal};
    font-size: 14px;
    font-weight: 500;
  }
  @media only screen and (max-width: ${CONSTS.responsive.md}px) {
    font-size: 12px;
  }
`;

export const Label = ({ required, help, children, ...props }) => {
  return (
    <StyledLabel {...props}>
      {children}
      {required ? (
        <div style={{ display: "inline-block", marginInlineStart: 4 }}> *</div>
      ) : null}
      {help ? (
        <Icon
          name="help"
          data-tip={help}
          style={{ marginRight: 7, opacity: 0.7, cursor: "pointer" }}
        />
      ) : null}
      {/* <ReactTooltip
        effect={"solid"}
        className="input-toolip"
        // backgroundColor={CONSTS.colors.grey[4]}
      /> */}
    </StyledLabel>
  );
};
