import React from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";

const Loading = ({ className, ...props }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    version="1.0"
    width="252px"
    height="69px"
    viewBox="0 0 128 35"
    className={className}
    {...props}
  >
    <g>
      <circle fill="#000000" fillOpacity="1" cx="17.5" cy="17.5" r="17.5" />
      <animate
        attributeName="opacity"
        dur="900ms"
        begin="0s"
        repeatCount="indefinite"
        keyTimes="0;0.167;0.5;0.668;1"
        values="0.3;1;1;0.3;0.3"
      />
    </g>
    <g>
      <circle fill="#000000" fillOpacity="1" cx="110.5" cy="17.5" r="17.5" />
      <animate
        attributeName="opacity"
        dur="900ms"
        begin="0s"
        repeatCount="indefinite"
        keyTimes="0;0.334;0.5;0.835;1"
        values="0.3;0.3;1;1;0.3"
      />
    </g>
    <g>
      <circle fill="#000000" fillOpacity="1" cx="64" cy="17.5" r="17.5" />
      <animate
        attributeName="opacity"
        dur="900ms"
        begin="0s"
        repeatCount="indefinite"
        keyTimes="0;0.167;0.334;0.668;0.835;1"
        values="0.3;0.3;1;1;0.3;0.3"
      />
    </g>
  </svg>
);

const StyledLoading = styled(Loading)`
    height: 12px;
    circle {
        fill: ${CONSTS.colors.info.normal};
    }
    ${(props) => `
        ${props.small ? `height: 8px;` : ``}
        ${props.large ? `height: 16px;` : ``}

        ${props.primary ? `
            circle {
                fill: ${CONSTS.colors.primary.normal};
            }
        ` : ``}

        ${props.success ? `
            circle {
                fill: ${CONSTS.colors.success.normal};
            }
        ` : ``}
        ${props.error ? `
            circle {
                fill: ${CONSTS.colors.error.normal};
            }
        ` : ``}
        ${props.warning ? `
            circle {
                fill: ${CONSTS.colors.warning.normal};
            }
        ` : ``}
        ${props.info ? `
            circle {
                fill: ${CONSTS.colors.grey[4]};
            }
        ` : ``}

        ${props.invert ? `
            circle {
                fill: white;
            }
        ` : ``}

    `}
`;

export default StyledLoading;
