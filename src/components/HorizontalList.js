// import React, { useState, useRef, useEffect } from "react";
// import styled from "styled-components";
// import CONSTS from "utils/constants";
// // import ScrollMenu from "react-horizontal-scrolling-menu";
// // import OverflowWrapper from "react-overflow-wrapper";
// // import { Swiper, SwiperSlide } from "swiper/react";
// // import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from "swiper";
// import { Flex, Icon } from "./";
// // import "swiper/swiper.scss";
// // import "swiper/components/navigation/navigation.scss";
// // import "swiper/components/pagination/pagination.scss";
// // import "swiper/components/scrollbar/scrollbar.scss";
// // SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

// const ls = [
//   // { title: "همه خوراک" },
//   // { title: "علوفه چمن / حبوبات" },
//   // { title: "علوفه های محصول غلات" },
//   // { title: "منابع انرژی" },
//   // { title: "چربی ها" },
//   // { title: "پروتئین گیاهی" },
//   // { title: "پروتئین حیوانی" },
//   // { title: "محصول جانبی / خوراک های دیگر" },
// ];
// // const navigation = {
// //   nextEl: ".swiper-button-next",
// //   prevEl: ".swiper-button-prev",
// // };
// function HorizontalList({ className, onItemChanged, list, ...props }) {
//   //-- STATES --//
//   const [active, setActive] = useState(0);

//   //-- HOOKS --//
//   useEffect(() => {
//     onItemChanged(list[active].id);
//   }, [active]);

//   return (
//     <div className={className}>
//       <Flex
//         onClick={() =>
//           active < list.length - 1 ? setActive(active + 1) : null
//         }
//         jc="center"
//         ai="center"
//         // className="swiper-arrow next"
//       >
//         <Icon name="back" />
//       </Flex>
//       <div className="swiper-cmp">
//         <Swiper
//           spaceBetween={15}
//           slidesPerView={"auto"}
//           onSlideChange={(e) => setActive(e.activeIndex)}
//           initialSlide={active}
//           //   navigation
//           observer={1}
//           observeParents={1}
//           parallax={1}
//           freeMode={1}
//           slideToClickedSlide={1}
//           //  navigation={{
//           //    nextEl: <div style={{width:100}}>sdfsdfsdf</div>,
//           //    prevEl: <div style={{width:100}}>sdfsdfsdf</div>,
//           //  }}
//           //  onSwiper={(swiper) => console.log(swiper)}
//         >
//           {list.map((v, k) => (
//             <SwiperSlide
//               key={k}
//               virtualIndex={k}
//               onClick={() => setActive(k)}
//               className={`swiper-item ${active == k ? "active" : ""}`}
//             >
//               {v.title_fa ?? v.title}
//             </SwiperSlide>
//           ))}
//         </Swiper>
//       </div>
//       <Flex
//         onClick={() => (active > 0 ? setActive(active - 1) : null)}
//         jc="center"
//         ai="center"
//         className="swiper-arrow prev"
//       >
//         <Icon name="forward" />
//       </Flex>
//     </div>
//   );
// }

// const StyledHorizontalList = styled(HorizontalList)`
//   direction: rtl;
//   .swiper-cmp {
//     display: inline-block;
//     width: calc(100% - 110px);
//     margin-right: 10px;
//   }
//   .swiper-item {
//     width: fit-content;
//     background: transparent;
//     text-align: center;
//     padding: 14px;
//     border-radius: 9px;
//     font-weight: 500;
//     font-family: "PeydaWeb";
//     font-size: 14px;
//     color: #6c757d;
//     cursor: pointer;
//     transition: ease background 0.25s, ease color 0.25s;
//     margin-left: 10px;
//     &:hover,
//     &.active {
//       background: ${CONSTS.colors.info.light};
//       color: ${CONSTS.colors.info.normal};
//     }
//   }
//   .swiper-arrow {
//     font-size: 10px;
//     background: #ffffff;
//     border: 1px solid #e9ecef;
//     border-radius: 9px;
//     width: 47px;
//     height: 47px;
//     cursor: pointer;
//     transition: ease background 0.25s, ease color 0.25s;
//     &:hover {
//       background: ${CONSTS.colors.info.normal};
//       color: white;
//     }
//     &.prev {
//       float: right;
//     }
//     &.next {
//       float: left;
//     }
//   }
// `;

// export default StyledHorizontalList;
