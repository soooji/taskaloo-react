import React, { useState } from "react";
import styled from "styled-components";
import { Card, Button, Flex, ButtonGroup } from "./";
import { Modal } from "react-responsive-modal";
import CONSTS from "../utils/constants";
import EditTask from "../containers/projects/EditTask";
import TaskApi from "./../api/task";

const STATUS = {
  1: "تسک جدید",
  2: "برنامه‌ریزی شده",
  3: "در حال انجام",
  4: "انجام شده",
};

function Task({ className, id, name, statusId, description, refresh }) {
  const [isMenuOpen, setisMenuOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  async function deleteTask() {
    try {
      if (!id) return;
      setIsLoading(true);
      await TaskApi.deleteTask(id);
      setIsLoading(false);
      refresh();
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  return (
    <div className={className}>
      <Modal
        open={isMenuOpen}
        onClose={() => setisMenuOpen(false)}
        center
        classNames={{
          modal: "normal-modal__container",
          overlay: "normal-modal__overlay",
          closeButton: "normal-modal__close-button",
        }}
      >
        <Flex className="modal-title-bar">
          <div className="modal-title-bar__text">{name}</div>
        </Flex>
        <EditTask id={id} refresh={refresh}/>
      </Modal>
      <Card className="card-style" key={name} loading={isLoading}>
        <Flex className="container" jc="space-between" ai="center">
          <div>
            <div className="name-style">
              {name} - {STATUS[statusId]}
            </div>
            <div className="description">{description}</div>
          </div>
          <Flex>
            <ButtonGroup>
              <Button
                onClick={() => deleteTask()}
                icon="trash_can"
                invert={1}
                error={true}
                nomargin
                className="meta__button"
                iconStyle={{ margin: 0, padding: 0 }}
              ></Button>
              <Button
                nomargin
                onClick={() => setisMenuOpen(true)}
                icon="back"
                invert={1}
                className="meta__button"
                iconStyle={{ margin: 0, padding: 0 }}
              ></Button>
            </ButtonGroup>
          </Flex>
        </Flex>
      </Card>
    </div>
  );
}
const StyledTask = styled(Task)`
  font-family: "PeydaWeb";
  .card-style {
    margin-bottom: 20px;
    width: 100%;
  }
  .description {
    font-size: 13px;
  }

  .container {
    /* background:red; */
  }
  .meta__button {
    margin: 0 10px 0 0;
    width: 42px;
    height: 42px;
  }
  .name-style {
    margin-bottom: 10px;
    font-weight: 600;
  }
  .members {
    background: ${CONSTS.colors.primary.light};
    border-radius: 15px;
    padding: 1px;
    color: ${CONSTS.colors.primary.normal};
    font-size: 13px;
    padding-left: 7px;
    margin-left: 10px;
  }
  .icon-style {
    background: ${CONSTS.colors.primary.midLight};
    border-radius: 15px;
    padding: 2px;
    margin-left: 2px;
  }
  .tags {
    background: ${CONSTS.colors.error.light};
    border-radius: 15px;
    padding: 1px;
    color: ${CONSTS.colors.error.normal};
    font-size: 13px;
    padding-left: 7px;
  }
  .icon-style2 {
    background: ${CONSTS.colors.error.midLight};
    border-radius: 15px;
    padding: 2px;
    margin-left: 2px;
  }
  .trash-style {
    background: ${CONSTS.colors.error.light};
    padding: 7px;
    font-size: 18px;
    color: ${CONSTS.colors.error.normal};
    border-radius: 7px;
    margin-left: 8px;
    cursor: pointer;
  }
  .back-style {
    background: ${CONSTS.colors.primary.light};
    padding: 7px;
    font-size: 18px;
    color: ${CONSTS.colors.primary.normal};
    border-radius: 7px;
    cursor: pointer;
  }
`;
export default StyledTask;

// {
//   /* <Flex>
//                             <Flex className="members">
//                                 <Icon className="icon-style" name="account"/>
//                                 {users ? users.map((u, o) => (
//                                     <div>{u.first_name+" "+u.last_name+","}</div>
//                                 )):null}
//                             </Flex>
//                             <Flex className="tags">
//                                 <Icon className="icon-style2" name="combo_chart"/>
//                                 {tags ? tags.map((t, p) => (
//                                     <div>{t.name+","}</div>
//                                 )):null}
//                             </Flex>
//                         </Flex> */
// }
