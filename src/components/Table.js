import React from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { Link } from "react-router-dom";
import { Loading, Flex } from "./";

const Table = ({
  columns,
  dataSource,
  className,
  style,
  loading,
  emptyText,
}) => {
  const handleRender = (col, record) => {
    if (col.render) {
      if (typeof col.render == "function") {
        return col.render(record[col.dataIndex], record);
      } else if (typeof col.render == "string") {
        switch (col.render) {
          case "link":
            return (
              <Link to={col.linkURL ? col.linkURL : ""}>
                {col.linkLabel ? col.linkLabel : "View"}
              </Link>
            );
          default:
            return record[col.dataIndex] ? record[col.dataIndex] : "";
        }
      }
    }
    return record[col.dataIndex];
  };

  return (
    <div className={className}>
      <table
        style={style}
        className="table-style"
        border="0"
        cellPadding="0"
        cellSpacing="0"
      >
        <thead>
          <tr>
            {columns.map((col, i) => (
              <td key={i} style={col.style}>
                {col.title}
              </td>
            ))}
          </tr>
        </thead>
        <tbody>
          {/* TODO: loading is not currectly shown */}
          {loading ? (
            <Flex
              style={{ width: "100%", margin: "50px auto" }}
              ai="center"
              jc="center"
            >
              <Loading />
            </Flex>
          ) : null}
          {!loading && dataSource.length == 0 ? (
            <Flex
              style={{ width: "100%", margin: "50px auto", fontSize: '1.1rem' }}
              ai="center"
              jc="center"
            >
              {emptyText ?? "لیست مورد نظر خالی است."}
            </Flex>
          ) : null}
          {dataSource &&
            dataSource.map((record, i) => (
              <tr key={i}>
                {columns.map((col, j) => {
                  return (
                    <td key={j} style={{ ...record.style, ...col.recordStyle }}>
                      {handleRender(col, record)}
                    </td>
                  );
                })}
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
};

const StyledTable = styled(Table)`
  width: 100%;
  font-family: "PeydaWeb";
  overflow: auto;
  ${(props) =>
    props.hasBorder
      ? `border: 1px solid ${CONSTS.colors.grey[8]};border-radius: 10px;`
      : ""};
  a {
    color: ${CONSTS.colors.info};
  }
  .table-style {
    width: 100%;
    border-collapse: separate;
    & > thead {
      font-size: 13px;
      font-weight: bold;
      color: ${CONSTS.colors.grey[4]};
      @media only screen and (max-width: ${CONSTS.responsive.md}px) {
        font-size: 11px;
      }
      > tr {
        > td {
          text-align: center;
          border-bottom: 1px solid ${CONSTS.colors.grey[8]};
          padding-top: 10px;
          padding-bottom: 10px;
        }
      }
    }
    & > tbody {
      font-size: 14px;
      font-weight: normal;
      @media only screen and (max-width: ${CONSTS.responsive.md}px) {
        font-size: 12px;
      }

      > tr {
        > td {
          border-bottom: 1px solid ${CONSTS.colors.grey[8]};
          margin-right: 5px;
          text-align: center;
          padding: 10px;
          line-height: 30px;
          border-left: none;
          border-right: none;
          &:last-child {
            width: 1%;
          }
        }
        &:last-child {
          > td {
            border: none;
          }
        }
      }
    }
  }
  ${(props) =>
    props.small &&
    `
            > thead {
                font-size: 13px;
                > tr{
                    > td{

                    }
                }
            }
            > tbody{
                font-size: 13px;
                > tr{
                    > td{
                        padding: 5px;
                        border-left: none;
                        border-right: none;
                        border-radius: 0 !important
                        font-weight: normal;
                    }
                }
            }
        `}
`;

export default StyledTable;
