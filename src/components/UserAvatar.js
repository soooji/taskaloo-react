import React from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { Flex } from "./";
import SystemLogo from "./../static/images/avatar.png";

function UserAvatar({ className, avatarUrl, fullName, ...props }) {
  return (
    <Flex
      jc="space-between"
      ai="center"
      className={className + " noselect"}
      {...props}
    >
      <img src={avatarUrl ? avatarUrl : SystemLogo} alt="تصویر پروفایل" />
      <div className="user-name">{fullName ?? "حساب کاربری"}</div>
    </Flex>
  );
}

const StyledUserAvatar = styled(UserAvatar)`
  cursor: pointer;
  border-radius: 100px;
  padding-left: 10px;
  padding-top: 3px;
  padding-bottom: 3px;
  padding-right: 3px;
  transition: ease 0.2s background;
  @media only screen and (max-width: ${CONSTS.responsive.md}px) {
    display: none;
  }
  img {
    width: 38px;
    width: 38px;
    object-fit: cover;
  }
  .user-name {
    font-family: ${CONSTS.fonts.normal};
    font-size: 14px;
    color: #000000;
    font-weight: 500;
    padding-right: 10px;
    transition: ease 0.2s color;
  }
  &:hover {
    background-color: ${CONSTS.colors.grey[9]};
    .user-name {
      color: ${CONSTS.colors.primary.normal};
    }
  }
`;

export default StyledUserAvatar;
