import React, { useState } from "react";
import styled from "styled-components";
import CONSTS, { terms } from "../utils/constants";
import { Field, Form, Formik } from "formik";
import { toast } from "react-toastify";
import {
  Flex,
  PartTitleBar,
  Fields,
  FormikContext,
  ButtonGroup,
  Button,
} from "./";

function FormGenerator({
  className,
  setValues,
  initialValues = {},
  onSubmit,
  onCancel,
  fields,
  showSubmitButton = true,
  submitButtonText = "تایید و ذخیره",
  cancelButtonText = "انصراف",
  buttonGroupStyles = {},
  disableAll,
  disableErrors,
  fulopacityondisabled,
  validationSchema,
  onErrorChange,
  hideDisableds = false,
  ...props
}) {
  //-- STATES --//
  const [errors, setErrors] = useState([]);

  //-- OTHER FUNCTIONS --//
  const getInputTypeComponent = (type) => {
    switch (type) {
      case "input":
        return Fields.Input;
      case "select":
        return Fields.Select;
      case "checkbox":
        return Fields.CheckBox;
      case "radio":
        return Fields.Radio;
      case "textarea":
        return Fields.TextArea;
      default:
        return Fields.Input;
    }
  };

  const setDisabledFieldsValueNull = async (vals) => {
    return vals;
  };

  return (
    <Formik
      {...props}
      validationSchema={validationSchema}
      enableReinitialize
      initialValues={initialValues}
      onSubmit={
        onSubmit
          ? async (values) => {
              if (errors) {
                if (errors.length >= 0) {
                  toast(terms.form.hasError, {
                    type: toast.TYPE.ERROR,
                  });
                  return;
                }
              }
              let res = await setDisabledFieldsValueNull(values);
              await onSubmit(res);
            }
          : null
      }
    >
      {(props) => {
        return (
          <Form onSubmit={props.handleSubmit} className={className}>
            {fields
              ? fields.map((row, kr) => {
                  const { errors, touched, isValid, dirty } = props;
                  setErrors(errors);
                  if (onErrorChange) {
                    onErrorChange(errors);
                  }
                  return (
                    <Flex
                      jc="space-between"
                      ai="flex-start"
                      className="form-row"
                      key={kr}
                    >
                      {row.map((field, kf) =>
                        !field.hidden && !(field.disabled && hideDisableds) ? (
                          field.meta ? (
                            field.meta
                          ) : field.isPartTitle ? (
                            <PartTitleBar
                              style={{ marginBottom: 20 }}
                              key={"" + kr + kf}
                              title={field.label}
                              {...field}
                            />
                          ) : (
                            <Field
                              key={"" + kr + kf}
                              component={getInputTypeComponent(field.inputType)}
                              labelnomargin={kr == 0}
                              half={field.half || row.length > 1 ? 1 : 0}
                              style={{ width: "100%" }}
                              disabled={disableAll}
                              fulopacityondisabled={
                                fulopacityondisabled ? 1 : 0
                              }
                              error={errors[field.name]}
                              // labelnomargin={1}
                              disableError={disableErrors}
                              {...field}
                            />
                          )
                        ) : null
                      )}
                    </Flex>
                  );
                })
              : null}
            {showSubmitButton ? (
              <ButtonGroup
                style={{
                  marginBottom: 15,
                  marginRight: 15,
                  marginTop: 15,
                  ...buttonGroupStyles,
                }}
              >
                <Button
                  // onClick={onSubmit ?? null}
                  loading={props.isSubmitting ? 1 : 0}
                  disabled={props.isSubmitting}
                  success={1}
                  icon="checkmark"
                  nomargin={1}
                >
                  {submitButtonText}
                </Button>
                {onCancel ? (
                  <Button
                    onClick={(e) => {
                      e.preventDefault();
                      onCancel();
                    }}
                    loading={props.isSubmitting ? 1 : 0}
                    disabled={props.isSubmitting}
                    info={1}
                    invert={1}
                    nomargin={1}
                  >
                    {cancelButtonText}
                  </Button>
                ) : null}
              </ButtonGroup>
            ) : null}
            {setValues ? <FormikContext setValues={setValues} /> : null}
          </Form>
        );
      }}
    </Formik>
  );
}
const StyleFormGenerator = styled(FormGenerator)`
  .form-row {
    @media only screen and (max-width: ${CONSTS.responsive.md}px) {
      flex-direction: column !important;
    }
  }
`;
export default StyleFormGenerator;
