import React, { useEffect, useContext, useState } from "react";
import styled from "styled-components";
import CONSTS from "../utils/constants";
import { Flex, Icon } from "./";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { NavBarContext } from "../utils/baseContext";
import { useSelect } from "react-select-search";

const menu = [
  {
    title: "داشبورد",
    icon: "home",
    url: "/",
  },
  {
    title: "پروژه ها",
    icon: "plus",
    url: "/projects",
  },
  {
    title: "تغییر رمز عبور",
    icon: "plus",
    url: "/change_password",
  },
  {
    title: "پروفایل",
    icon: "plus",
    url: "/profile",
  },

];

function NavBar({ className, ...props }) {
  //-- HOOKS --//
  const location = useLocation();
  const navbar = useContext(NavBarContext);
  // console.log("navar:", NavBarContext.isMenuOpen)
  // console.log("navar:", navbar.isMenuOpen)
  const usersi = {
    title: "کاربران",
    icon: "plus",
    url: "/users",
  }
  const usersii = {
    title: "آمار",
    icon: "plus",
    url: "/stats",
  }
  const [isAdmin, setIsAdmin] = useState(false);
  useEffect(() => {
    let res = localStorage.getItem("user");
    if (res) {
      res = JSON.parse(res);
      console.log(res);
      if (res.is_admin) {
        setIsAdmin(true);
      }
    }
  }, [])
  return (
    <div className={`${className} ${navbar.isMenuOpen ? "opened-menu" : ""}`}>
      {menu.map((v, k) => (
        <NavItem
          active={
            v.url == "/"
              ? location.pathname == "/"
              : location.pathname == "/reports/new"
                ? v.url == "/reports/new"
                : location.pathname.includes(v.url)
          }
          key={k}
          style={v.hasBottomSpacer ? { marginBottom: 15 } : {}}
          {...v}
        />))}
      {isAdmin ?
      <>
        <NavItem
          active={
            usersi.url == "/"
              ? location.pathname == "/"
              : location.pathname == "/reports/new"
                ? usersi.url == "/reports/new"
                : location.pathname.includes(usersi.url)
          }
          style={usersi.hasBottomSpacer ? { marginBottom: 15 } : {}}
          {...usersi}
        />
        <NavItem
          active={
            usersii.url == "/"
              ? location.pathname == "/"
              : location.pathname == "/reports/new"
                ? usersii.url == "/reports/new"
                : location.pathname.includes(usersii.url)
          }
          style={usersii.hasBottomSpacer ? { marginBottom: 15 } : {}}
          {...usersii}
        />
        </>
         : null}

    </div>
  );
}

const StyledNavBar = styled(NavBar)`
  right: 0;
  height: calc(100vh - 150px);
  /* height:100vh; */
  top: 100px;
  width: 240px;
  border-left: 1px solid ${CONSTS.colors.grey[8]};
  position: fixed;
  padding-left: 22px;
  padding-right: 22px;
  box-sizing: border-box;
  @media only screen and (max-width: ${CONSTS.responsive.md}px) {
    position: fixed;
    right: -100%;
    bottom: 0;
    top: 60px;
    padding-top: 3vh;
    box-sizing: border-box;
    width: 100%;
    height: calc(100vh - 60px);
    z-index: 1111;
    overflow-y: auto;
    background-color: rgba(255, 255, 255, 0.3);
    backdrop-filter: blur(2px);
    transition: ease right 0.25s, linear background-color 0.1s 0.15s;
  }
  &.opened-menu {
    right: 0;
    background-color: rgba(255, 255, 255, 0.9);
  }
`;

function NavItemCmp({ className, title, icon, url, active, ...props }) {
  const navbar = useContext(NavBarContext);
  return (
    <Link
      to={url}
      onClick={() => navbar.toggleMenu(false)}
      className={className}
    >
      <Flex
        style={{ ...props.style }}
        jc="flex-start"
        ai="center"
        className={` nav-item ${active ? "active" : ""}`}
      >
        {icon ? <Icon name={icon} large={1} /> : null}
        <div className="nav-item__title">{title}</div>
      </Flex>
    </Link>
  );
}

const NavItem = styled(NavItemCmp)`
  .nav-item {
    color: #6c757d;
    height: 44px;
    box-sizing: border-box;
    border-radius: ${CONSTS.borderRadius.normal};
    padding-right: 14px;
    transition: ease color 0.2s, ease background 0.2s;
    margin-bottom: 5px;
    @media only screen and (max-width: ${CONSTS.responsive.md}px) {
      height: 53px;
    }
    &__title {
      font-family: ${CONSTS.fonts.normal};
      font-weight: 500;
      font-size: 14px;
      letter-spacing: 0;
      text-align: right;
      margin-right: 12px;
    }
    &.active {
      color: ${CONSTS.colors.primary.normal};
      background: ${CONSTS.colors.primary.light};
    }
    @media only screen and (min-width: ${CONSTS.responsive.md}px) {
      &:hover {
        color: ${CONSTS.colors.primary.normal};
        background: ${CONSTS.colors.primary.light};
      }
    }
  }
`;

export default StyledNavBar;
