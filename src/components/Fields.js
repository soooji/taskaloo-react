import React from "react";
import { Input, InputInfo, Label, Flex } from "./";
import SelectSearch from "react-select-search";

// field, // { name, value, onChange, onBlur }
// form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
// meta,

const NormalInput = ({ field, form, meta, error, disableError, ...props }) => {
  return (
    <div
      className={`input-item ${props.inline ? "inline-field" : ""}`}
      style={{
        width: props.half ? "calc(50% - 7.5px)" : "100%",
        opacity: props.fulopacityondisabled
          ? 1
          : form.isSubmitting || props.disabled
          ? 0.5
          : 1,
        marginBottom: !disableError ? 10 : 5,
      }}
    >
      <Flex
        fd={`${props.inline ? "row" : "column"}`}
        jc="space-between"
        style={{ width: "100%" }}
      >
        {props.label && (
          <Label
            htmlFor={field.name}
            required={!props.inline ? props.required : false}
            help={props.help}
            nomargin={props.labelnomargin}
            noFill={props.inline}
          >
            {props.inline ? (
              <>
                <b>{props.label}</b>
                <div>{props.sublabel}</div>
              </>
            ) : (
              props.label
            )}
          </Label>
        )}
        <Input
          {...field}
          {...props}
          id={field.name}
          className={`${props.inline ? "inline-input" : ""}`}
          disabled={form.isSubmitting || props.disabled}
        />
      </Flex>
      {!disableError ? <InputInfo title={error} /> : null}
    </div>
  );
};

const Select = ({ field, form, error, meta, disableError, ...props }) => {
  return (
    <div
      className={`input-item ${props?.inline ? "inline-field" : ""}`}
      style={{
        width: props?.half ? "calc(50% - 7.5px)" : "100%",
        opacity: props?.fulopacityondisabled
          ? 1
          : form?.isSubmitting || props?.disabled
          ? 0.5
          : 1,
        marginBottom: !disableError ? 10 : 5,
      }}
    >
      <Flex
        fd={`${props?.inline ? "row" : "column"}`}
        jc="space-between"
        style={{ width: "100%" }}
      >
        {props.label && (
          <Label
            htmlFor={field?.name}
            required={!props?.inline ? props?.required : false}
            help={props?.help}
            nomargin={props?.labelnomargin}
            noFill={props?.inline}
          >
            {props?.inline ? (
              <>
                <b>{props?.label}</b>
                <div>{props?.sublabel}</div>
              </>
            ) : (
              props?.label
            )}
          </Label>
        )}
        <SelectSearch
          {...field}
          {...props}
          id={field?.name}
          disabled={form?.isSubmitting || props?.disabled}
          className={`select-search ${props?.inline ? "inline-input" : ""} ${
            !props?.disableError ? "force-opacity" : ""
          }`}
          onChange={(v) => {
            form?.setFieldValue(field?.name, v);
          }}
        />
      </Flex>
      {!disableError ? <InputInfo title={error} /> : null}
    </div>
  );
};

const Check = ({ field, form, meta, error, disableError, ...props }) => (
  <div
    className="input-item"
    style={{
      width: props.half ? "calc(50% - 7.5px)" : "100%",
      opacity: props.fulopacityondisabled
        ? 1
        : form.isSubmitting || props.disabled
        ? 0.5
        : 1,
      marginBottom: !disableError ? 10 : 5,
    }}
  >
    <Flex
      fd={`${props.inline ? "row" : "column"}`}
      jc="space-between"
      style={{ width: "100%" }}
    >
      {props.label && !props.disableTopLabel && (
        <Label
          htmlFor={field.name}
          required={props.required}
          help={props.help}
          nomargin={props.labelnomargin}
        >
          {props.label}
        </Label>
      )}
      <Input.Checkbox
        {...field}
        {...props}
        id={field.name}
        disabled={form.isSubmitting || props.disabled}
      />
    </Flex>
    {!disableError ? <InputInfo title={error} /> : null}
  </div>
);

const Radio = ({ field, form, meta, error, disableError, ...props }) => (
  <div
    className="input-item"
    style={{
      width: props.half ? "calc(50% - 7.5px)" : "100%",
      opacity: props.fulopacityondisabled
        ? 1
        : form.isSubmitting || props.disabled
        ? 0.5
        : 1,
      marginBottom: !disableError ? 10 : 5,
    }}
  >
    <Flex
      fd={`${props.inline ? "row" : "column"}`}
      jc="space-between"
      style={{ width: "100%" }}
    >
      {props.label && (
        <Label
          htmlFor={field.name}
          required={props.required}
          help={props.help}
          nomargin={props.labelnomargin}
        >
          {props.label}
        </Label>
      )}
      <Input.Radio
        {...field}
        {...props}
        id={field.name}
        disabled={form.isSubmitting || props.disabled}
      />
    </Flex>
    {!disableError ? <InputInfo title={error} /> : null}
  </div>
);

const TextArea = ({ field, form, meta, error, disableError, ...props }) => (
  <div
    className="input-item"
    style={{
      width: props.half ? "calc(50% - 7.5px)" : "100%",
      opacity: props.fulopacityondisabled
        ? 1
        : form.isSubmitting || props.disabled
        ? 0.5
        : 1,
      marginBottom: !disableError ? 10 : 5,
    }}
  >
    <Flex
      fd={`${props.inline ? "row" : "column"}`}
      jc="space-between"
      style={{ width: "100%" }}
    >
      {props.label && (
        <Label
          htmlFor={field.name}
          required={props.required}
          help={props.help}
          nomargin={props.labelnomargin}
        >
          {props.label}
        </Label>
      )}
      <Input.Textarea
        {...field}
        {...props}
        id={field.name}
        disabled={form.isSubmitting || props.disabled}
      />
    </Flex>
    {!disableError ? <InputInfo title={error} /> : null}
  </div>
);

export default {
  Input: NormalInput,
  Select: Select,
  CheckBox: Check,
  Radio: Radio,
  TextArea: TextArea,
};

// <Label
//    htmlFor="firstName"
//    required
//    help="نام و نام خانوداگی شما مطابق چیزی که در شناسنامه شما موجود است."
//    nomargin
//  >
//    نام و نام خانوادگی
//  </Label>
// <InputInfo title="عنوان گزارش را وارد کنید" />
