import React from "react";
import styled from "styled-components";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";

//tabs schema:
// tabs : [...,{title: 'tab 1 title',content: <div></div>},...]

function TabsCmp({
  className,
  tabs,
  keepTabs = false,
  activeTabClassName = "tabs__tab--active",
  activePanelClassName = "tabs__panel--active",
  tabClassName = "tabs__tab",
  panelClassName = "tabs__panel",
  tabListClassName = "tabs__tabList",
  ...props
}) {
  return (
    <Tabs
      className={className}
      forceRenderTabPanel={keepTabs}
      selectedTabClassName={activeTabClassName}
      selectedTabPanelClassName={activePanelClassName}
      {...props}
    >
      <TabList className={tabListClassName}>
        {tabs.map((v, k) => (
          <Tab key={k} className={tabClassName}>
            {v.title}
          </Tab>
        ))}
      </TabList>

      {tabs.map((v, k) => (
        <TabPanel key={k} className={panelClassName}>
          {v.content}
        </TabPanel>
      ))}
    </Tabs>
  );
}

const StyledTabs = styled(TabsCmp)``;

export default StyledTabs;
