import React from "react";
import styled from "styled-components";
import { Button, Flex, Input } from "./";

function MiniSearch({
  className,
  inputProps,
  rightButtonText,
  rightButtonProps,
  ...props
}) {
  return (
    <Flex jc="flex-start" ai="flex-start" className={className} {...props}>
      {rightButtonText ? (
        <Button
          small={1}
          invert={1}
          nomargin={1}
          style={{ marginLeft: 10 }}
          {...rightButtonProps}
        >
          {rightButtonText}
        </Button>
      ) : null}
      <Input {...inputProps} />
    </Flex>
  );
}

const StyleMiniSearch = styled(MiniSearch)``;

export default StyleMiniSearch;
