import React, { useState } from "react";
import styled from "styled-components";
import StatsList from "./Stats"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams,
  } from "react-router-dom";
function Stats(){
    let match = useRouteMatch();
    console.log(match)
    return (
        
        <div>
            <Router>
                <Switch>
                    <Route path={match.path}>
                        <StatsList/>
                    </Route>
                </Switch>
            </Router>
        </div>
    )

}
const StyledStats = styled(Stats)``;
export default StyledStats;