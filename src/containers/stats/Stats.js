import React, { useEffect, useState } from "react";
import styled from "styled-components";
// import { Chart } from "react-charts";
import Chart from "react-apexcharts";
import { Card, Button, Flex, ButtonGroup ,ChartPartCmp} from "../../components";
import CONSTS from "../../utils/constants";
import { Link } from "react-router-dom";
// import StatsReq from "api/statistics";
import StatApi from "./../../api/stat"
import { getCachedUser } from "../../utils/functions";
import { random } from "lodash";
import moment from "moment";
// import persify from "persify";


function StatsList({ className, ...props }) {
  const [stats, setStats] = useState([]);
  const [isLoading, setIsLoading] = useState(false); //TODO: Handle initial loading


    // const stats = {
    //     projects:{
    //         week:[
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "3"
    
    //             },
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "4"
    
    //             },
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "5"
    
    //             },
    //         ],
    //         month:[
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "3"
    
    //             },
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "4"
    
    //             },
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "5"
    
    //             },
    //         ]
           
    //     },
    //     tasks:{
    //         week:[
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "3"
    
    //             },
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "4"
    
    //             },
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "5"
    
    //             },
    //         ],
    //         month:[
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "3"
    
    //             },
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "4"
    
    //             },
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "5"
    
    //             },
    //         ]
           
    //     },
    //     users:{
    //         week:[
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "3"
    
    //             },
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "4"
    
    //             },
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "5"
    
    //             },
    //         ],
    //         month:[
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "3"
    
    //             },
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "4"
    
    //             },
    //             {
    //                 date: "2021-02-11T13:27:00.000Z",
    //                 count: "5"
    
    //             },
    //         ]
           
    //     },
    // }
    useEffect(() => {
        getStats();
      }, []);
    async function getStats() {
        try {
          setIsLoading(true);
          let r = await StatApi.getStats();
          setStats(r.data);
          setIsLoading(false);
        } catch (e) {
          setIsLoading(false);
          console.log(e);
        }
      }


    //-- OTHER FUNCTIONS --//

    return (
        
        <div className={className}>
            {/* {console.log(stats.projects)} */}
           < ChartPartCmp
           title="گزارش گیری پروژه ها"
           stats={stats.projects}
           />
           < ChartPartCmp
           title="گزارش گیری تسک ها"
           stats={stats.tasks}
           />
           < ChartPartCmp
           title="گزارش گیری کاربران"
           stats={stats.users}
           />
        </div>
    );
}

const StyledStatsList = styled(StatsList)`
  flex: 2;
  margin-bottom: 30px;
  .chart-container {
    /* background:red;   */
    margin-right: 30px;
    margin-left: 30px;
    height: 390px;
    margin-top: 10px;
  }
`;



export default StyledStatsList;
