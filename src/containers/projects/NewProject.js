import React, { useState } from "react";
import styled from "styled-components";
import { getCachedUser } from "../../utils/functions";
import { FormGenerator, Card } from "../../components";
import ProjectApi from "../../api/project";

function NewProject({ className, onToggleModal, getList }) {
  //-- STATES --//
  const [isLoading, setIsLoading] = useState(false);
  const [project, setProject] = useState({
    name: null,
    description: null,
  });

  //-- HOOKS --//

  //-- API REQUESTS --//
  async function postProject() {
    try {
      setIsLoading(true);
      await ProjectApi.postProject({ ...project });
      setIsLoading(false);
      onToggleModal(false);
      getList();
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  return (
    <div className={className}>
      <FormGenerator
        fields={[
          [
            {
              name: "name",
              placeholder: "عنوان",
              label: "عنوان پروژه ",
              required: true,
            },
          ],
          [
            {
              name: "description",
              placeholder: "توضیحات",
              label: "توضیحات پروژه",
              required: true,
              inputType: "textarea",
            },
          ],
        ]}
        initialValues={project}
        showSubmitButton={true}
        submitButtonText="تایید"
        buttonGroupStyles={{ marginRight: 0, marginBottom: 0 }}
        onSubmit={(values) => postProject()}
        setValues={setProject}
      />
    </div>
  );
}

const StyledNewProject = styled(NewProject)``;
export default StyledNewProject;
