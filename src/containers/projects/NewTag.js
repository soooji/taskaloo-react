import React, { useState } from "react";
import styled from "styled-components";
import { getCachedUser } from "../../utils/functions";
import { FormGenerator, Card } from "../../components";
import TaskApi from "./../../api/task";

function NewTag({ className, refresh, onClose }) {
  //-- STATES --//
  const [isLoading, setIsLoading] = useState(false);
  const [tag, setTag] = useState({
    name: null,
  });

  //-- HOOKS --//

  //-- API REQUESTS --//
  async function postTag() {
    try {
      setIsLoading(true);
      await TaskApi.postTag({ ...tag });
      if (refresh) {
        refresh();
      }
      onClose();
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  return (
    <div className={className}>
      <FormGenerator
        fields={[
          [
            {
              name: "name",
              label: "عنوان تگ",
              required: true,
            },
          ],
        ]}
        showSubmitButton={true}
        submitButtonText="تایید "
        buttonGroupStyles={{ marginRight: 0, marginBottom: 0 }}
        onSubmit={() => postTag()}
        setValues={(v) => setTag(v)}
      />
    </div>
  );
}

const StyledNewTask = styled(NewTag)``;
export default StyledNewTask;
