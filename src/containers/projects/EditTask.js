/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { getCachedUser } from "../../utils/functions";
import { Modal } from "react-responsive-modal";
import SelectSearch from "react-select-search";
import { FormGenerator, Button, Card, Flex, Icon, Label } from "../../components";
import CONSTS from "../../utils/constants";
import ProjectApi from "../../api/project";
import TaskApi from "../../api/task";
import UsersApi from "../../api/user";
import NewTag from "./NewTag";
import moment from "moment";

function EditTask({
  className,
  id,
  refresh,
  disableComments = false,
  onEnd = () => { },
}) {
  //-- STATES --//
  const [isLoading, setIsLoading] = useState(false);
  const [newTagOpen, setNewTagOpen] = useState(false);
  const [comment, setComment] = useState({
    message: null
  })


  const [task, setTask] = useState({
    description: null,
    end_date: null,
    estimated_time: null,
    name: null,
    project_id: null,
    start_date: null,
    status_id: 1,
    story_point: null,
    comments: [],
    tags: [],
    users: [],
  });

  const [projects, setProjects] = useState([]);
  const [users, setUsers] = useState([]);
  const [tags, setTags] = useState([]);

  const [selectedUsers, setSelectedUsers] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);

  //-- HOOKS --//
  useEffect(() => {
    getProjects();
    getUsers();
    getTags();
  }, []);

  useEffect(() => {
    if (id) {
      getTask();
    }
  }, [id]);

  //-- API REQUESTS --//
  async function getTask() {
    try {
      setIsLoading(true);

      let r = await TaskApi.getTasks(`/${id}`);
      let taskItem = { ...r.data };

      taskItem.users = r?.data?.users?.map((v) => v.id);
      taskItem.tags = r?.data?.tags?.map((v) => v.id);
      if (taskItem.start_date) {
        taskItem.start_date = taskItem.start_date.slice(0, 16);
      }
      if (taskItem.end_date) {
        taskItem.end_date = taskItem.end_date.slice(0, 16);
      }

      setSelectedUsers([...taskItem.users]);
      setSelectedTags([...taskItem.tags]);

      setTask(taskItem);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  async function putTask() {
    try {
      setIsLoading(true);
      let body = {
        ...task,
        users: selectedUsers,
        tags: selectedTags,
        status_id: parseInt(task?.status_id),
        start_date: task?.start_date
          ? moment(new Date(task.start_date)).format("YYYY-MM-DD HH:mm:ss")
          : null,
        end_date: task?.end_date
          ? moment(new Date(task.end_date)).format("YYYY-MM-DD HH:mm:ss")
          : null,
      };
      delete body.project;
      delete body.comments;
      delete body.id;
      delete body.create_date

      if (id) {

        await TaskApi.putTask({ ...body }, `/${id}`);
      } else {
        await TaskApi.postTask({ ...body });
      }
      setIsLoading(false);
      if (refresh) {
        refresh();
      }
      onEnd();
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  async function postComment() {
    try {
      setIsLoading(true);
      await TaskApi.postComment({ ...comment }, `/${id}/comment`);
      getTask();
      setIsLoading(false);
      setComment({ message: null })
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  async function getProjects() {
    try {
      setIsLoading(true);
      let r = await ProjectApi.getProjects();
      setProjects(r.data);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  async function getUsers() {
    try {
      setIsLoading(true);
      let r = await UsersApi.getUser();
      setUsers(r.users);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  async function getTags() {
    try {
      setIsLoading(true);
      let r = await TaskApi.getTags();
      setTags(r?.data);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  const toggleItem = (type = "tag", v) => {
    let temp = [];
    temp = type === "tag" ? [...selectedTags] : [...selectedUsers];
    let index = temp.indexOf(v);
    index < 0 ? temp.push(v) : temp.splice(index, 1);
    type === "tag" ? setSelectedTags([...temp]) : setSelectedUsers([...temp]);
  };

  const getItem = (type = "tag", id) => {
    let targetList = type === "tag" ? [...tags] : [...users];
    let index = -1;
    targetList.forEach((v, k) => {
      if (v?.value == id || v?.id == id) {
        index = k;
        return;
      }
    });
    return index > -1 ? targetList[index] : null;
  };

  return (
    <div className={className}>
      <Modal
        open={newTagOpen}
        onClose={() => setNewTagOpen(false)}
        center
        styles={{
          modal: { maxWidth: 500 },
        }}
        classNames={{
          modal: "normal-modal__container",
          overlay: "normal-modal__overlay",
          closeButton: "normal-modal__close-button",
        }}
      >
        <Flex className="modal-title-bar">
          <div className="modal-title-bar__text">تگ جدید</div>
        </Flex>
        <NewTag refresh={getTags} onClose={() => setNewTagOpen(false)} />
      </Modal>
      <FormGenerator
        disableAll={isLoading}
        fields={[
          [
            {
              name: "name",
              label: "عنوان تسک",
              required: true,
              inline: 1,
              value: task?.name,
            },
          ],
          [
            {
              name: "status_id",
              label: "وضعیت",
              required: true,
              inline: 1,
              inputType: "select",
              options: [
                {
                  name: "تسک جدید",
                  value: "1",
                },
                {
                  name: "تسک شروع نشده",
                  value: "2",
                },
                {
                  name: "تسک در حال انجام",
                  value: "3",
                },
                {
                  name: "تسک انجام شده",
                  value: "4",
                },
              ],
            },
          ],
          [
            {
              name: "project_id",
              label: "پروژه",
              required: true,
              inline: 1,
              inputType: "select",
              options: projects.map((v) => ({ name: v?.name, value: v?.id })),
            },
          ],
          [
            {
              meta: (
                <Flex
                  fd={`row`}
                  jc="space-between"
                  style={{ width: "100%", marginLeft: 14 }}
                >
                  <Label
                    htmlFor={"userss"}
                    required={true}
                    noFill={true}
                    style={{ fontWeight: "bold" }}
                  >
                    تخصیص داده شده به
                  </Label>
                  <SelectSearch
                    id={"userss"}
                    name={"userss"}
                    disabled={isLoading}
                    className={`select-search inline-input`}
                    onChange={(v) => {
                      toggleItem("user", v);
                    }}
                    {...{
                      name: "userses",
                      placeholder: "جستجو در کاربران...",
                      options: [
                        // { name: "انتخاب کنید...", value: "0" },
                        ...users.map((v) => ({
                          name: v?.first_name + " " + v?.last_name,
                          value: v?.id,
                        })),
                      ],
                      // value: "0",
                    }}
                  />
                </Flex>
              ),
            },
          ],
          [
            {
              meta: (
                <div className="chips">
                  {selectedUsers.map((v) => {
                    let val = getItem("user", v);
                    return (
                      <div className="chip">
                        <div>
                          {(val?.first_name ?? "") +
                            " " +
                            (val?.last_name ?? "")}
                        </div>
                        <Icon
                          onClick={() => toggleItem("user", val.id)}
                          name="trash_can"
                          className="chip__icon"
                        ></Icon>
                      </div>
                    );
                  })}
                </div>
              ),
            },
          ],

          //
          //
          //

          [
            {
              meta: (
                <Flex
                  fd={`row`}
                  jc="space-between"
                  style={{ width: "100%", marginLeft: 14 }}
                >
                  <Label
                    htmlFor={"tagss"}
                    // required={true}
                    noFill={true}
                    style={{ fontWeight: "bold" }}
                  >
                    تگ‌ها
                    *
                    <Button
                      small
                      success
                      nomargin
                      className="newtag"
                      link
                      icon="plus"
                      style={{ marginRight: 10 }}
                      onClick={(e) => {
                        e.preventDefault();
                        setNewTagOpen(true);
                      }}
                    >
                      تگ جدید
                    </Button>
                  </Label>
                  <SelectSearch
                    id={"tagss"}
                    name={"tagss"}
                    disabled={isLoading}
                    className={`select-search inline-input`}
                    onChange={(v) => {
                      toggleItem("tag", v);
                    }}
                    {...{
                      name: "tagss",
                      placeholder: "جستجو در تگ‌ها...",
                      options: [
                        ...tags.map((v) => ({
                          name: v?.name,
                          value: v?.id,
                        })),
                      ],
                    }}
                  />
                </Flex>
              ),
            },
          ],
          [
            {
              meta: (
                <div className="chips">
                  {selectedTags.map((v) => {
                    let val = getItem("tag", v);
                    return val ? (
                      <div className="chip">
                        <div>{val.name}</div>
                        <Icon
                          onClick={() => toggleItem("tag", val.id)}
                          name="trash_can"
                          className="chip__icon"
                        ></Icon>
                      </div>
                    ) : (
                        <div></div>
                      );
                  })}
                </div>
              ),
            },
          ],

          //
          //
          //

          [
            {
              name: "estimated_time",
              label: "زمان تخمینی",
              placeholder: "به دقیقه",
              inline: true,
            },
          ],
          [
            {
              name: "story_point",
              label: "امتیاز",
              placeholder: "عدد مثبت",
              inline: true,
            },
          ],
          [
            {
              name: "start_date",
              label: "روز و زمان شروع",
              inline: true,
              type: "datetime-local",
            },
          ],
          [
            {
              name: "end_date",
              label: "روز و زمان پایان",
              inline: true,
              type: "datetime-local",
            },
          ],
          [
            {
              name: "description",
              placeholder: "توضیحات",
              label: "توضیحات تسک",
              inputType: "textarea",
            },
          ],
        ]}
        initialValues={task}
        showSubmitButton={true}
        submitButtonText="تایید و ذخیره"
        buttonGroupStyles={{ marginRight: 0, marginBottom: 0, marginTop: -20 }}
        onSubmit={(values) => putTask()}
        setValues={setTask}
        className="form-style"
      />
      {!disableComments ? (
        <>
          <div className="comments">ارسال دیدگاه</div>
          <FormGenerator
            fields={[
              [
                {
                  name: "message",
                  placeholder: "پیام خود را یادداشت کنید...",
                  label: "",
                  required: true,
                  inputType: "textarea",
                },
              ],
            ]}
            showSubmitButton={true}
            submitButtonText="ارسال"
            buttonGroupStyles={{
              marginRight: 0,
              marginBottom: 0,
              marginTop: -25,
            }}
            initialValues={comment}
            setValues={setComment}
            onSubmit={(values) => postComment()}
          />
          <div>
            {task?.comments
              ? task.comments.map((v, k) => (
                <Card
                  className="card-style"
                  containerStyle={{ backgroundColor: "#E9ECEF" }}
                >
                  <div>
                    <Flex
                      className="info-container"
                      fd="row"
                      jc="space-between"
                    >
                      <div className="info">
                        {v?.first_name + " " + v?.last_name}
                      </div>
                      <div className="date">
                        {moment(v?.update_time).fromNow()}
                      </div>
                    </Flex>
                    {v?.message}
                  </div>
                </Card>
              ))
              : null}
          </div>
        </>
      ) : null}
    </div>
  );
}

const StyledEditTask = styled(EditTask)`
  font-family: "PeydaWeb";
  direction: rtl;
  .chips {
    margin: 10px;
  }
  .chip {
    display: inline-block;
    border-radius: 20px;
    background-color: ${CONSTS.colors.primary.light};
    color: ${CONSTS.colors.primary.normal};
    padding: 5px 10px 5px 5px;
    margin-left: 10px;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    align-content: center;
    margin-bottom: 10px;
    div {
      font-size: 13px;
      display: inline-block;
      margin-left: 5px;
      font-weight: 400;
    }
    &__icon {
      cursor: pointer;
      width: 25px;
      text-align: center;
      height: 25px;
      padding-top: 3px;
      box-sizing: border-box;
      display: flex;
      align-items: center;
      justify-content: center;
      align-content: center;
      border-radius: 10px;
      display: inline-block;
      background-color: ${CONSTS.colors.error.light};
      color: ${CONSTS.colors.error.normal};
    }
  }
  .comments {
    font-weight: 400;
    font-size: 15px;
    margin-bottom: 10px;
    padding-right: 20px;
  }
  .form-style {
    border-bottom: 1px solid #e9ecef;
    padding-bottom: 40px;
    margin-bottom: 30px;
  }
  .newtag {
    margin-top: -8px;
    color: ${CONSTS.colors.success.normal};
    background-color: ${CONSTS.colors.success.light};
    padding-top: 8px;
    padding-bottom: 8px;
  }
  .card-style {
    direction: rtl;
    margin-top: 40px;
    margin-bottom: 10px;
    /* background:#0B0C0D; */
  }
  .info-container {
    margin-bottom: 20px;
  }
  .date {
    color: ${CONSTS.colors.grey[4]};
  }
  .info {
    color: ${CONSTS.colors.success.normal};
  }
`;
export default StyledEditTask;

// [
// {
//   name: "users",
//   placeholder: "جستجو در کاربران...",
//   label: "تخصیص داده شده به",
//   // search: true,
//   required: true,
//   closeOnSelect: false,
//   printOptions: "on-focus",
//   multiple: true,
//   inline: 1,
//   inputType: "select",
//   options: users.map((v) => ({
//     name: v?.first_name + " " + v?.last_name,
//     value: v?.id,
//   })),
//   // value: selectedUsers,
//   // onChangeValue: (v) => {
//   //   setSelectedUsers(v);
//   // },
// },
// ],
// [
//   {
//     name: "tags",
//     label: (
//       <ButtonGroup>
//         <div>تگ های تسک</div>
//         <Button
//           small
//           success
//           nomargin
//           className="newtag"
//           link
//           icon="plus"
//           onClick={(e) => {
//             e.preventDefault();
//             setNewTagOpen(true);
//           }}
//         >
//           تگ جدید
//         </Button>
//       </ButtonGroup>
//     ),
//     placeholder: "جستجو در تگ‌ها...",
//     // search: true,
//     required: true,
//     closeOnSelect: false,
//     printOptions: "on-focus",
//     inline: 1,
//     inputType: "select",
//     multiple: true,
//     options: tags.map((v) => ({ name: v?.name, value: v?.id })),
//     // value: selectedTags,
//     // onChangeValue: (v) => {
//     //   setSelectedTags(v);
//     // },
//   },
// ],

// <Fields.Select
//   {...{
//     name: "userses",
//     placeholder: "جستجو در کاربران...",
//     label: "تخصیص داده شده به",
//     // search: true,
//     required: true,
//     closeOnSelect: false,
//     printOptions: "on-focus",
//     multiple: true,
//     inline: 1,
//     inputType: "select",
//     options: users.map((v) => ({
//       name: v?.first_name + " " + v?.last_name,
//       value: v?.id,
//     })),
//     value: selectedUsers,
//     onChangeValue: (v) => {
//       setSelectedUsers(v);
//     },
//   }}
// />
