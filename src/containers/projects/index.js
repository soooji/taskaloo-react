import React, { useState } from "react";
import styled from "styled-components";
import ProjectsList from "./projectsList";
import TasksList from "./TasksList";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useRouteMatch,
} from "react-router-dom";
function Dashboard() {
  let match = useRouteMatch();
  console.log(match);
  return (
    // <div>
      <Router>
        <Switch>
          <Route exact path={match.path}>
            <ProjectsList />
          </Route>
          <Route path={`${match.path}/:id`}>
            <TasksList />
          </Route>
        </Switch>
      </Router>
    // </div>
  );
}
const StyledDashboard = styled(Dashboard)``;
export default StyledDashboard;
