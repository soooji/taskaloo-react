import React, { useState, useEffect } from "react";
import styled from "styled-components";
import {
  Card,
  Button,
  Flex,
  Task,
  FormGenerator,
  Loading,
  ButtonGroup,
} from "../../components";
import { useParams, useHistory } from "react-router-dom";
import { Modal } from "react-responsive-modal";
import EditTask from "./EditTask";
import ProjectApi from "../../api/project";

function TasksList({ className }) {
  const [isNewTask, setIsNewTask] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const params = useParams();

  const [project, setProject] = useState(null);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    let res = localStorage.getItem("user");
    if (res) {
      res = JSON.parse(res);
      console.log(res);
      if (res.is_admin) {
        setIsAdmin(true);
      }
    }
  }, [])

  useEffect(() => {
    if (params?.id) {
      getProject(params?.id);
    }
  }, [params]);

  //-- API REQUESTS --//
  async function getProject(id) {
    try {
      setIsLoading(true);
      let r = await ProjectApi.getProjects(`${id ?? params?.id}`);
      setProject(r.project);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  async function deleteProject() {
    try {
      if (!params?.id) return;
      setIsLoading(true);
      await ProjectApi.deleteProject(`${params?.id}`);
      setIsLoading(false);
      history.push("/projects");
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  async function putProject() {
    try {
      if (!params?.id) return;
      setIsLoading(true);
      await ProjectApi.putProject({ ...project }, `/${params?.id}`);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  return (
    <div className={className}>
      <Modal
        open={isNewTask}
        onClose={() => setIsNewTask(false)}
        center
        classNames={{
          modal: "normal-modal__container",
          overlay: "normal-modal__overlay",
          closeButton: "normal-modal__close-button",
        }}
      >
        <Flex className="modal-title-bar">
          <div className="modal-title-bar__text">تسک جدید</div>
        </Flex>
        <EditTask
          onEnd={() => setIsNewTask(false)}
          disableComments
          refresh={() => getProject(params?.id)}
        />
      </Modal>
      {isAdmin ?
        <Card
          nopadding={1}
          // nostyle={1}
          title="اطلاعات پروژه"
          style={{ width: "100%" }}
          meta={
            <ButtonGroup>
              <Button
                onClick={deleteProject}
                icon="trash_can"
                error={1}
                small={1}
                nomargin={1}
              >
                حذف پروژه
            </Button>
              <Button
                onClick={() => history.goBack()}
                info={1}
                small={1}
                nomargin={1}
              >
                بازگشت
            </Button>
            </ButtonGroup>
          }
        >
          <Card loading={isLoading} nostyle={1} style={{ width: "100%" }}>
            <FormGenerator
              fields={[
                [
                  {
                    name: "name",
                    placeholder: "عنوان",
                    label: "عنوان پروژه ",
                    required: true,
                  },
                ],
                [
                  {
                    name: "description",
                    placeholder: "توضیحات",
                    label: "توضیحات پروژه",
                    required: true,
                    inputType: "textarea",
                  },
                ],
              ]}
              initialValues={project}
              showSubmitButton={true}
              submitButtonText="تایید"
              buttonGroupStyles={{ marginRight: 0, marginBottom: 0 }}
              onSubmit={(values) => putProject()}
              setValues={setProject}
              className="form-style"
            />
          </Card>
          <div className="divider"></div>

          <Card
            // nopadding={1}
            nostyle={1}
            title="تسک ها"
            style={{ width: "100%" }}
            meta={
              <Button
                onClick={() => setIsNewTask(true)}
                icon="plus_math"
                success={1}
                small={1}
                nomargin={1}
              >
                تسک جدید
            </Button>
            }
          >
            {project?.tasks ? (
              project.tasks.length === 0 ? (
                <Flex jc="center" className="empty">
                  {isLoading ? (
                    <Loading />
                  ) : (
                      <div>لیست تسک‌های پروژه خالی است!</div>
                    )}
                </Flex>
              ) : (
                  project.tasks.map((v, k) => (
                    <Task
                      key={k}
                      id={v.id}
                      refresh={getProject}
                      name={v.name}
                      description={v.description}
                      users={v.users}
                      statusId={v.status_id}
                      tags={v.tags}
                    />
                  ))
                )
            ) : null}
          </Card>
        </Card>
        :
        <Card
          nopadding={1}
          // nostyle={1}
          title="اطلاعات پروژه"
          style={{ width: "100%" }}
          meta={
            <Button
              onClick={() => history.goBack()}
              info={1}
              small={1}
              nomargin={1}
            >
              بازگشت
            </Button>
          }
        >
          <Card loading={isLoading} nostyle={1} style={{ width: "100%" }}>
            <FormGenerator
              fields={[
                [
                  {
                    name: "name",
                    placeholder: "عنوان",
                    label: "عنوان پروژه ",
                    required: true,
                  },
                ],
                [
                  {
                    name: "description",
                    placeholder: "توضیحات",
                    label: "توضیحات پروژه",
                    required: true,
                    inputType: "textarea",
                  },
                ],
              ]}
              initialValues={project}
              disableAll={true}
              fulopacityondisabled={true}
              showSubmitButton={false}
              // submitButtonText="تایید"
              buttonGroupStyles={{ marginRight: 0, marginBottom: 0 }}
              // onSubmit={(values) => putProject()}
              setValues={setProject}
              className="form-style"
            />
          </Card>
          <div className="divider"></div>

          <Card
            // nopadding={1}
            nostyle={1}
            title="تسک ها"
            style={{ width: "100%" }}
            meta={
              <Button
                onClick={() => setIsNewTask(true)}
                icon="plus_math"
                success={1}
                small={1}
                nomargin={1}
              >
                تسک جدید
            </Button>
            }
          >
            {project?.tasks ? (
              project.tasks.length === 0 ? (
                <Flex jc="center" className="empty">
                  {isLoading ? (
                    <Loading />
                  ) : (
                      <div>لیست تسک‌های پروژه خالی است!</div>
                    )}
                </Flex>
              ) : (
                  project.tasks.map((v, k) => (
                    <Task
                      key={k}
                      id={v.id}
                      refresh={getProject}
                      name={v.name}
                      description={v.description}
                      users={v.users}
                      statusId={v.status_id}
                      tags={v.tags}
                    />
                  ))
                )
            ) : null}
          </Card>
        </Card>
      }
    </div>
  );
}
const StyledTasksList = styled(TasksList)`
  font-family: "PeydaWeb";
  .form-style {
    /* padding-bottom: 40px; */
    /* margin-bottom: 30px; */
  }
  .divider {
    border-bottom: 1px solid #e9ecef;
    width: 100%;
  }
  .empty {
    text-align: center;
    font-size: 1.1rem;
    margin-top: 30px;
    margin-bottom: 50px;
  }
`;
export default StyledTasksList;
