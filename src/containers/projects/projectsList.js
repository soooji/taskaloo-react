import React, { useState, useEffect } from "react";
import styled from "styled-components";
import {
  Card,
  Button,
  Flex,
  GroupCard,
  AnimationLoading,
  NewItemCard,
} from "../../components";
import { Modal } from "react-responsive-modal";
import NewProject from "./NewProject";
// import { getCachedUser } from "../../utils/functions";
import ProjectApi from "../../api/project";

import CONSTS from "../../utils/constants";

function ProjectsList({ className }) {
  //-- STATES --//
  const [isLoading, setIsLoading] = useState(false); //TODO: Handle initial loading
  const [projects, setProjects] = useState([]);
  const [isNewGroupOpen, setIsNewGroupOpen] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    let res = localStorage.getItem("user");
    if (res) {
      res = JSON.parse(res);
      console.log(res);
      if (res.is_admin) {
        setIsAdmin(true);
      }
    }
  }, [])

  //-- HOOKS --//
  useEffect(() => {
    getProjects();
  }, []);

  //-- API REQUESTS --//
  async function getProjects() {
    try {
      setIsLoading(true);
      let r = await ProjectApi.getProjects();
      setProjects(r.data);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  return (
    <div className={className}>
      <Modal
        open={isNewGroupOpen}
        onClose={() => setIsNewGroupOpen(false)}
        center
        classNames={{
          modal: "normal-modal__container",
          overlay: "normal-modal__overlay",
          closeButton: "normal-modal__close-button",
        }}
      >
        <Flex className="modal-title-bar">
          <div className="modal-title-bar__text">پروژه جدید</div>
        </Flex>
        <NewProject
          onToggleModal={(state) => setIsNewGroupOpen(state)}
          getList={() => getProjects()}
        />
      </Modal>
      {/* {isAdmin ? */}
      <Card
        nopadding={1}
        nostyle={1}
        title="پروژه ها"
        style={{ width: "100%" }}
        meta={isAdmin ?
          <Button
            onClick={() => setIsNewGroupOpen(true)}
            icon="plus_math"
            success={1}
            small={1}
            nomargin={1}
          >
            پروژه جدید
        </Button> : null
        }
      >
        {/* TODO: Fill empty list with 'Create New Item' */}

        <div className="cards-grid">
          {projects.map((v, k) => (
            <GroupCard
              key={k}
              title={v.name}
              description={v.description}
              link={`/projects/${v.id}`}
            />
          ))}
          {isLoading ? (
            new Array(8).fill(0).map((v, k) => <AnimationLoading height="200px" key={k} />)
          ) : (
              isAdmin ? <NewItemCard
                title="پروژه جدید"
                onClick={() => setIsNewGroupOpen(true)}
              /> : null
            )}
        </div>
      </Card>
      {/* :
        <Card
          nopadding={1}
          nostyle={1}
          title="پروژه ها"
          style={{ width: "100%" }}
        >

          <div className="cards-grid">
            {projects.map((v, k) => (
              <GroupCard
                key={k}
                title={v.name}
                description={v.description}
                link={`/projects/${v.id}`}
              />
            ))}
          </div>
        </Card>
      } */}


    </div>
  );
}

const StyledProjectsList = styled(ProjectsList)`
  .cards-grid {
    display: grid;
    grid-gap: 10px 10px;
    grid-template-columns: calc(33% - 10px) calc(33% - 10px) calc(33% - 10px);
    @media (max-width: ${CONSTS.responsive.xl}px) {
      grid-template-columns: calc(50% - 10px) calc(50% - 10px);
    }
    @media (max-width: ${CONSTS.responsive.lg}px) {
      grid-template-columns: calc(50% - 10px) calc(50% - 10px);
    }
    @media (max-width: ${CONSTS.responsive.sm}px) {
      grid-template-columns: calc(100% - 15px);
    }
  }
`;
export default StyledProjectsList;
