import React, { useState, useEffect } from "react";
import styled from "styled-components";
import {
  Card,
  Button,
  Flex,
  GroupCard,
  AnimationLoading,
  NewItemCard,
  MiniTask,
  LoadingSpecial,
} from "../../components";
import { Modal } from "react-responsive-modal";
import { getCachedUser } from "../../utils/functions";
import CONSTS from "../../utils/constants";
import TaskApi from "./../../api/task";

function DashboardItems({ className }) {
  const [tasks, setTasks] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getTasks();
  }, []);

  async function getTasks() {
    try {
      setIsLoading(true);
      let r = await TaskApi.getTasks();
      setTasks(r.data);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  return (
    <div className={className}>
      {isLoading ? (
        <div className="loadings">
          <LoadingSpecial />
        </div>
      ) : null}
      <Card
        containerStyle={{ padding: 10 }}
        className="card_style"
        title="جدید"
        headerClassName={"header_style"}
      >
        {tasks["1"]
          ? tasks["1"].map((v, k) => (
            <MiniTask
              refresh={() => getTasks()}
              task={v}
              name={v.name}
              users={v.users}
              tags={v.tags}
              id={v.id}
            />
          ))
          : null}
      </Card>
      <Card
        containerStyle={{ padding: 10 }}
        className="card_style"
        title="برنامه‌ریزی شده"
        headerClassName={"header_style"}
      >
        {tasks["2"]
          ? tasks["2"].map((v, k) => (
            <MiniTask
              refresh={() => getTasks()}
              task={v}
              name={v.name}
              users={v.users}
              tags={v.tags}
              id={v?.id}
            />
          ))
          : null}
      </Card>
      <Card
        containerStyle={{ padding: 10 }}
        className="card_style"
        title="در حال انجام"
        headerClassName={"header_style"}
      >
        {tasks["3"]
          ? tasks["3"].map((v, k) => (
            <MiniTask
              refresh={() => getTasks()}
              task={v}
              id={v.id}
              name={v.name}
              users={v.users}
              tags={v.tags}
            />
          ))
          : null}
      </Card>
      <Card
        containerStyle={{ padding: 10 }}
        className="card_style"
        title="انجام شده"
        headerClassName={"header_style"}
      >
        {tasks["4"]
          ? tasks["4"].map((v, k) => (
            <MiniTask
              refresh={() => getTasks()}
              task={v}
              name={v.name}
              users={v.users}
              tags={v.tags}
              id={v.id}
            />
          ))
          : null}
      </Card>
    </div>
  );
}
const StyledDashboardItems = styled(DashboardItems)`
  display: flex;
  height: auto;
  @media only screen and (max-width: ${CONSTS.responsive.md}px) {
      flex-direction:column;
    }
  /* margin-top: 20px; */
  .loadings {
    position: fixed;
    left: calc(50vw - 50px);
    top: calc(30vh - 50px);
    z-index: 1111;
  }
  .card_style {
    flex: 1;
    margin-right: 7px;
    margin-left: 7px;
    display: block;
    > .card-container {
      min-height: calc(100vh - 60px);
    }
    @media only screen and (max-width: ${CONSTS.responsive.md}px) {
      > .card-container {
        min-height: 100px;
      }
    }
  }
  .header_style {
    text-align: center;
    margin-top: 0;
    /* border: 1px solid #e9ecef; */
    text-align: center;
    border-radius: ${CONSTS.borderRadius.outside};
    /* margin-bottom: 10px; */
    margin-left: auto;
    margin-right: auto;
  }
`;
export default StyledDashboardItems;
