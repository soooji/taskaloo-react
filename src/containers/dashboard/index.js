import React, { useState } from "react";
import styled from "styled-components";
import DashboardItems from "./DashboardItems"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams,
} from "react-router-dom";

function Dashboard(){
    let match = useRouteMatch();
    console.log(match)
    return (
        <div>
            <Router>
                <Switch>
                    <Route path={match.path}>
                        <DashboardItems/>
                    </Route>
                </Switch>
            </Router>
        </div>
    )

}
const StyledDashboard = styled(Dashboard)``;
export default StyledDashboard;