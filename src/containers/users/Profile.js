import React, { useState, useEffect } from "react";
import styled from "styled-components";
import {
    Card,
    Button,
    Flex,
    GroupCard,
    AnimationLoading,
    NewItemCard,
    FormGenerator
} from "../../components";
import { Modal } from "react-responsive-modal";
import { getCachedUser } from "../../utils/functions";
import CONSTS from "../../utils/constants";
import { useParams, useHistory } from "react-router-dom";
import UserApi from "../../api/user"

function Profile({ className }) {
    const [isLoading, setIsLoading] = useState(false);
    const [user, setUser] = useState([]);
    const history = useHistory();

    useEffect(() => {
        getUser();
    }, []);

    async function getUser() {
        try {
            setIsLoading(true);
            let r = await UserApi.getUserProfile();
            console.log(r)
            setUser(r);
            localStorage.setItem("user", JSON.stringify(r))
            setIsLoading(false);
        } catch (e) {
            setIsLoading(false);
            console.log(e);
        }
    }
    async function putUser() {
        try {
            setIsLoading(true);
            let r = await UserApi.putUserProfile(user);
            getUser()
            console.log(r)
            setUser(r);
            setIsLoading(false);
        } catch (e) {
            setIsLoading(false);
            console.log(e);
        }
    }



    return (
        <div className={className}>
            <Card
                // nopadding={1}
                // nostyle={1}
                title="ویرایش پروفایل"
                style={{ width: "100%" }}

            >
                <FormGenerator
                    fields={[
                        [
                            {
                                name: "username",
                                label: "نام کاربری",
                                required: true,
                            },
                            {
                                name: "email",
                                label: "ایمیل",
                                required: true,
                            },
                        ],
                        [
                            {
                                name: "first_name",
                                label: "نام",
                                required: true,
                            },
                            {
                                name: "last_name",
                                label: "نام خانوادگی",
                                required: true,
                            },
                        ],
                    ]}
                    initialValues={user}
                    showSubmitButton={true}
                    submitButtonText="تایید و ذخیره"
                    buttonGroupStyles={{ marginRight: 0, marginBottom: 0 }}
                    onSubmit={(values) => putUser()}
                    setValues={setUser}
                />
            </Card>

        </div>
    );

}
const StyledProfile = styled(Profile)`
`;
export default StyledProfile;
