import React, { useState, useEffect } from "react";
import styled from "styled-components";
import {
  Card,
  Button,
  Flex,
  GroupCard,
  AnimationLoading,
  NewItemCard,
  FormGenerator,
  Table,
} from "../../components";
import { Modal } from "react-responsive-modal";
import { getCachedUser } from "../../utils/functions";
import CONSTS from "../../utils/constants";
import UserApi from "../../api/user";
import { Link } from "react-router-dom";

function UsersList({ className }) {
  const [isLoading, setIsLoading] = useState(false);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  async function getUsers() {
    try {
      setIsLoading(true);
      let r = await UserApi.getUser();
      console.log(r);
      setUsers(r.users);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }
  const columns = [
    {
      dataIndex: "username",
      title: "نام کاربری",
    },
    {
      dataIndex: "id",
      title: "نام و نام خانوادگی",
      render: (i, obj) => (obj ? obj.first_name + " " + obj.last_name : ""),
    },
    {
      dataIndex: "id",
      title: "جزئیات",
      render: (id) => (
        <Link to={`/users/${id}`}>
          <Button small nomargin>
            مشاهده
          </Button>
        </Link>
      ),
    },
  ];

  return (
    <div className={className}>
      <Card nopadding={1} title="کاربران" style={{ width: "100%" }}>
        <Table
          loading={isLoading ? 1 : 0}
          // emptyText="لیست گزارش‌های شما خالی است."
          columns={columns}
          dataSource={isLoading ? [] : users}
          style={{ minWidth: 600 }}
        />
      </Card>
    </div>
  );
}
const StyledUsersList = styled(UsersList)``;
export default StyledUsersList;
