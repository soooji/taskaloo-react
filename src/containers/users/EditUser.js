import React, { useState, useEffect } from "react";
import styled from "styled-components";
import {
    Card,
    Button,
    Flex,
    GroupCard,
    AnimationLoading,
    NewItemCard,
    FormGenerator,
} from "../../components";
import { Modal } from "react-responsive-modal";
import { getCachedUser } from "../../utils/functions";
import CONSTS from "../../utils/constants";
import { useParams, useHistory } from "react-router-dom";
import UserApi from "../../api/user";

function EditUser({ className }) {
    const [isLoading, setIsLoading] = useState(false);
    const [user, setUser] = useState(false);

    const history = useHistory();
    const params = useParams();
    useEffect(() => {
        if (params?.id) {
            getUser(params?.id);
        }
    }, [params]);

    async function getUser(id) {
        try {
            setIsLoading(true);
            let r = await UserApi.getUser(`${id}`);
            console.log(r.user);
            setUser(r.user);
            setIsLoading(false);
        } catch (e) {
            setIsLoading(false);
            console.log(e);
        }
    }

    async function putUser() {
        try {
          if (!params?.id) return;
          setIsLoading(true);
          await UserApi.putUser({ ...user, is_admin: user.is_admin ? 1 : 0 }, `/${params?.id}`);
          setIsLoading(false);
        } catch (e) {
          setIsLoading(false);
          console.log(e);
        }
      }


    return (
        <div className={className}>
            <Card
                title="ویرایش کاربر"
                style={{ width: "100%" }}
            >
                <FormGenerator
                    fields={[
                        [
                            {
                                name: "username",
                                label: "نام کاربری",
                                required: true,
                                disabled: true
                            },
                            {
                                name: "email",
                                label: "ایمیل",
                                required: true,
                            },
                        ],
                        [
                            {
                                name: "first_name",
                                label: "نام",
                                required: true,
                            },
                            {
                                name: "last_name",
                                label: "نام خانوادگی",
                                required: true,
                            },
                        ],
                        [
                            {
                                name: "is_admin",
                                label: "کاربر مدیر",
                                inputType:"checkbox",
                                half:true
                            },
                        ]
                    ]}
                    initialValues={user}
                    showSubmitButton={true}
                    submitButtonText="تایید و ذخیره"
                    buttonGroupStyles={{ marginRight: 0, marginBottom: 0 }}
                    setValues={setUser}
                    onSubmit={(values) => putUser()}
                    // disableAll={true}
                    fulopacityondisabled={true}
                />
            </Card>
        </div>
    );
}
const StyledEditUser = styled(EditUser)``;
export default StyledEditUser;
