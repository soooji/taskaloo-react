import React, { useState } from "react";
import styled from "styled-components";
import UsersList from "./UsersList"
import EditUser from "./EditUser"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams,
  } from "react-router-dom";
function Users(){
    let match = useRouteMatch();
    console.log(match)
    return (
        
        <div>
            <Router>
                <Switch>
                    <Route path={`${match.path}/:id`}>
                        <EditUser/>
                    </Route>
                    <Route path={match.path}>
                        <UsersList/>
                    </Route>
                </Switch>
            </Router>
        </div>
    )

}
const StyledUsers = styled(Users)``;
export default StyledUsers;