import React, { useState, useEffect } from "react";
import styled from "styled-components";
import {
  Card,
  Button,
  Flex,
  GroupCard,
  AnimationLoading,
  NewItemCard,
  FormGenerator,
} from "../../components";
import { toast } from "react-toastify";
import { Modal } from "react-responsive-modal";
import { getCachedUser } from "../../utils/functions";
import CONSTS from "../../utils/constants";
import ChangePass from "../../api/changePass"

function EditUser({ className }) {
  const [pass, setPass] = useState(false);
  const [isLoading, setIsLoading] = useState(false);


  async function changePass() {
    if (pass.password == pass.passwordAgain) {
      try {
        setIsLoading(true);
        let r = await ChangePass.change_pass({ oldPassword: pass.oldPassword, password: pass.password });
        console.log(r)
        setPass(r);
        setIsLoading(false);
      } catch (e) {
        setIsLoading(false);
        console.log(e);
      }
    }
    else {
      toast("رمز عبور با تکرار آن برابر نیست!", {
        autoClose: false,
        type: toast.TYPE.ERROR,
      });

    }

  }
  return (
    <div className={className}>
      <Card
        // nopadding={1}
        // nostyle={1}
        title="تغییر رمز عبور"
        style={{ width: "100%" }}
      >
        <FormGenerator
          fields={[
            [
              {
                name: "oldPassword",
                label: "رمز فعلی",
                required: true,
                // type: "password",
                half: true,
              },
            ],
            [
              {
                half: true,
                name: "password",
                label: "رمز جدید",
                required: true,
                // type: "password",

              },
            ],
            [
              {
                half: true,
                name: "passwordAgain",
                label: "تکرار رمز جدید",
                required: true,
                // type: "password",
              }
            ],
          ]}
          showSubmitButton={true}
          submitButtonText="تایید و ذخیره"
          buttonGroupStyles={{ marginRight: 0, marginBottom: 0 }}
          onSubmit={(values) => changePass()}
          setValues={setPass}
        />
      </Card>
    </div>
  );
}
const StyledEditUser = styled(EditUser)``;
export default StyledEditUser;
