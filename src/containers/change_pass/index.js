import React, { useState } from "react";
import styled from "styled-components";
import ChangePass from "./ChangePass"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams,
  } from "react-router-dom";
function Change_Pass(){
    let match = useRouteMatch();
    console.log(match)
    return (
        
        <div>
            <Router>
                <Switch>
                    <Route path={match.path}>
                        <ChangePass/>
                    </Route>
                </Switch>
            </Router>
        </div>
    )

}
const StyledChange_Pass = styled(Change_Pass)``;
export default StyledChange_Pass;