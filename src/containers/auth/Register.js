import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Card, Button, Flex, Label, Input } from "./../../components";
import CONSTS from "../../utils/constants";
import SelectSearch from "react-select-search";
import { Link, Redirect, useHistory } from "react-router-dom";
import AuthReq from "../../api/auth";
// import validate from "validate.js";
import { toast } from "react-toastify";

const constraints = {
  username: {
    presence: {
      message: "^نام کاربری خود را وارد کنید",
    },
    email: {
      message: "^نام کاربری به درستی وارد نشده",
    },
  },
  email: {
    presence: {
      message: "^ایمیل خود را وارد کنید",
    },
    email: {
      message: "^ایمیل به درستی وارد نشده",
    },
  },
  password: {
    presence: {
      message: "^رمز عبور خود را وارد کنید",
    },
    length: {
      minimum: 4,
      message: "^رمز عبور باید حداقل ۴ کاراکتر باشد",
    },
  },
  first_name: {
    presence: {
      message: "^نام خود را وارد کنید",
    },
  },
  last_name: {
    presence: {
      message: "^نام خانوادگی خود را وارد کنید",
    },
  },
  answer: {
    presence: {
      message: "^جواب خود را وارد کنید",
    },
  },
};

function Register({ className, ...props }) {
  //-- STATES --//
  const [isRegistered, setIsRegistered] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [userId, setUserId] = useState(null);
  const history = useHistory();
  //// register parameters
  const [userObject, setUserObject] = useState({
    username: null,
    email: null, //string
    password: null, //string
    first_name: null, //string
    last_name: null, //string
    answer: null,
  });

  //-- API REQUESTS --//
  async function registerUser() {
    // let validationResult = validate({...userObject},constraints );

    // if (validationResult) {
    //   toast(validationResult[Object.keys(validationResult)[0]][0], {
    //     type: toast.TYPE.ERROR,
    //   });
    //   return;
    // }
    try {
      setIsLoading(true);
      let res = await AuthReq.Register({ ...userObject });
      if (res?.user?.data) {
        setUserId(res.user.data);
        setIsRegistered(true);
      }
      toast("ثبت نام با موفقیت انجام شد، وارد شوید.", {
        type: toast.TYPE.SUCCESS,
      });
      history.push("/auth");
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  }

  //-- OTHER FUNCTIONS --//
  return (
    <div className={className}>
      {isRegistered ? (
        <Redirect
          to={`/auth/confirm-email?user=${userId}&email=${userObject?.email?.key}`}
        />
      ) : null}
      <Card title="ثبت نام" titleIcon="account" style={{ width: "100%" }}>
        <div {...props}>
          <form onSubmit={(e) => e.preventDefault()} style={{ width: "100%" }}>
            <Label htmlFor="username" style={{ margin: 0 }}>
              نام کاربری
            </Label>
            <Input
              id="username"
              name="username"
              value={userObject.username}
              onChange={(e) =>
                setUserObject({ ...userObject, username: e.target.value })
              }
            />
            <Label htmlFor="firstName" style={{ margin: 0 }}>
              نام
            </Label>
            <Input
              id="firstName"
              name="firstName"
              value={userObject.first_name}
              onChange={(e) =>
                setUserObject({ ...userObject, first_name: e.target.value })
              }
            />

            <Label htmlFor="lastName">نام خانوادگی</Label>
            <Input
              id="lastName"
              name="lastName"
              value={userObject.last_name}
              onChange={(e) =>
                setUserObject({ ...userObject, last_name: e.target.value })
              }
            />

            <Label htmlFor="email">ایمیل</Label>
            <Input
              id="email"
              name="email"
              dir="ltr"
              type="email"
              value={userObject?.email}
              onChange={(e) =>
                setUserObject({
                  ...userObject,
                  email: e.target.value,
                })
              }
            />

            <Label htmlFor="password">رمز عبور</Label>
            <Input
              id="password"
              name="password"
              dir="ltr"
              type="password"
              value={userObject?.password}
              onChange={(e) =>
                setUserObject({ ...userObject, password: e.target.value })
              }
            />
            <Label htmlFor="answer" style={{ margin: 0 }}>
              اولین شغل شما چه بوده است؟
            </Label>
            <Input
              id="answer"
              name="answer"
              value={userObject.answer}
              onChange={(e) =>
                setUserObject({ ...userObject, answer: e.target.value })
              }
            />

            <Flex ai="center">
              <Button
                success
                icon="checkmark"
                onClick={registerUser}
                loading={isLoading ? 1 : 0}
              >
                ثبت نام
              </Button>
            </Flex>
          </form>
        </div>
      </Card>
      <Flex jc="space-between" ai="center" className="switch-part">
        <Flex fd="column" jc="center" ai="flex-start">
          <div className="switch-part__title">ثبت نام کرده‌اید؟</div>
          <div className="switch-part__subtitle">
            وارد ناحیه کاربری خود شوید
          </div>
        </Flex>
        <Link to="/auth">
          <Button success={1} invert={1} nomargin={1}>
            ورود
          </Button>
        </Link>
      </Flex>
    </div>
  );
}

const StyledRegister = styled(Register)`
  width: 100%;
  padding-top: 100px;
  padding-bottom: 100px;
  .switch-part {
    width: 100%;
    padding: 30px;
    box-sizing: border-box;
    font-family: ${CONSTS.fonts.normal};
    color: #000000;
    &__title {
      font-weight: 500;
      font-size: 16px;
    }
    &__subtitle {
      font-weight: 300;
      font-size: 12px;
    }
  }
`;

export default StyledRegister;

// {/* <InputInfo title="عنوان گزارش را وارد کنید" /> */}

// const [activityProvince, setActivityProvince] = useState(null);
// const [activityCity, setActivityCity] = useState(null);
// const [baseUserId, setBaseUserId] = useState(null);
// const [firstName, setFirstName] = useState(null);
// const [lastName, setLastName] = useState(null);
// const [skill, setSkill] = useState(null);
// const [skillTitle, setSkillTitle] = useState(null);
