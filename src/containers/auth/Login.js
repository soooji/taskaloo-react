import React, { useState } from "react";
import styled from "styled-components";
import { Card, Button, Flex, Label, Input, InputInfo } from "./../../components";
import CONSTS from "../../utils/constants";
import { errorHandler } from "../../utils/functions"
import { Link, Redirect } from "react-router-dom";
import AuthReq from "../../api/auth";
import { toast } from "react-toastify";

function Login({ className, ...props }) {
  //-- STATES --//
  const [loggedIn, setLoggedIn] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [username, setUsername] = useState("");
  const [pass, setPass] = useState("");
  const [checkErrors, setCheckErrors] = useState(false);


  //-- API REQUESTS --//
  async function logIn() {
    try {
      setCheckErrors(true);
      if (!username || !pass) return;

      setIsLoading(true);
      let r = await AuthReq.Login({
        username: username,
        password: pass,
      });
      setLoggedIn(true);
    } catch (e) {
      // errorHandler(e)
      setIsLoading(false);
      console.log(e);
    }
  }



  return (
    <Flex fd="column" ai="center" jc="center" className={className}>
      {loggedIn ? <Redirect to="/" /> : null}

      <Card title="ورود" titleIcon="account" style={{ width: "100%" }}>
        <div {...props}>
          <form
            onSubmit={(e) => e.preventDefault()}
            style={{ width: "100%" }}
            novalidate
          >
            <Label htmlFor="username" style={{ margin: 0 }}>
              نام کاربری
            </Label>
            <Input
              id="username"
              dir="ltr"
              textAlign="left"
              type="username"
              onChange={(e) => setUsername(e.target.value)}
            />

            {checkErrors && !username ? (
              <InputInfo title="نام کاربری خود را وارد کنید" />
            ) : null}

            <Label htmlFor="pass">
              <Flex jc="space-between" ai="center">
                <div>رمز عبور</div>
                <Link
                  to="/auth/forget-pass"
                  style={{ color: CONSTS.colors.primary.normal, fontWeight: 400 }}
                >
                  فراموشی رمز عبور
                </Link>
              </Flex>
            </Label>
            <Input
              id="pass"
              dir="ltr"
              type="password"
              onChange={(e) => setPass(e.target.value)}
            />
            {checkErrors && !pass ? (
              <InputInfo title="رمز عبور خود را وارد کنید" />
            ) : null}

            <Flex ai="center">
              <Button
                onClick={logIn}
                loading={isLoading ? 1 : 0}
                success
                icon="checkmark"
              >
                ورود به حساب
              </Button>
            </Flex>
          </form>
        </div>
      </Card>
      <Flex jc="space-between" ai="center" className="switch-part">
        <Flex fd="column" jc="center" ai="flex-start">
          <div className="switch-part__title">عضو تسکولو نیستید؟</div>
          <div className="switch-part__subtitle">
            به صورت رایگان ثبت نام کنید
          </div>
        </Flex>
        <Link to="/auth/register">
          <Button success={1} invert={1} nomargin={1}>
            ثبت نام
          </Button>
        </Link>
      </Flex>
    </Flex>
  );
}

const StyledLogin = styled(Login)`
  width: 100%;
  min-height: 100vh;
  /* margin-top: 30vh; */
  .switch-part {
    width: 100%;
    padding: 30px;
    box-sizing: border-box;
    font-family: ${CONSTS.fonts.normal};
    color: #000000;
    &__title {
      font-weight: 500;
      font-size: 16px;
    }
    &__subtitle {
      font-weight: 300;
      font-size: 12px;
    }
  }
`;

export default StyledLogin;
