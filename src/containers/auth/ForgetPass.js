import React, { useState, useEffect } from "react";
import styled from "styled-components";
import {
    Card,
    Button,
    Flex,
    GroupCard,
    AnimationLoading,
    NewItemCard,
    FormGenerator,
    InputInfo,
    Input,
    Label
} from "../../components";
import { Modal } from "react-responsive-modal";
import { getCachedUser } from "../../utils/functions";
import CONSTS from "../../utils/constants";
import { useParams, useHistory,Link } from "react-router-dom";
import AuthReq from "../../api/auth"
import { toast } from "react-toastify";


function ForgetPass({ className ,...props}) {
    const [isLoading, setIsLoading] = useState(false);
    const [changePassObj, setChangePassObj] = useState([]);
    const [username, setUsername] = useState("");
    const [pass, setPass] = useState("");
    const [answer, setAnswer] = useState("");
    const history = useHistory();


    async function changePassword() {
        // try {
        //     setIsLoading(true);
        //     let r = await AuthReq.ForgetPass(changePassObj);
        //     console.log(r)
        //     setIsLoading(false);
        // } catch (e) {
        //     setIsLoading(false);
        //     console.log(e);
        // }

        try {
            setIsLoading(true);
            console.log(username)
            console.log(answer)
            console.log(pass)
            let res = await AuthReq.ForgetPass({username:username,answer:answer,password:pass});
            // if (res?.user?.data) {
            //   setUserId(res.user.data);
            //   setIsRegistered(true);
            // }
            toast("ثبت نام با موفقیت انجام شد، وارد شوید.", {
              type: toast.TYPE.SUCCESS,
            });
            history.push("/auth");
            setIsLoading(false);
          } catch (e) {
            setIsLoading(false);
            console.log(e);
          }
    }



    return (
        <Flex fd="column" ai="center" jc="center" className={className}>
            <Card title="فراموشی رمز عبور" titleIcon="account" style={{ width: "100%" }}>
                <div {...props}>
                    <form
                        onSubmit={(e) => e.preventDefault()}
                        style={{ width: "100%" }}
                        novalidate
                    >
                        <Label htmlFor="username" style={{ margin: 0 }}>
                            نام کاربری
                        </Label>
                        <Input
                            id="username"
                            dir="ltr"
                            textAlign="left"
                            type="username"
                            onChange={(e) => setUsername(e.target.value)}
                        />

                        {/* {checkErrors && !username ? (
                            <InputInfo title="نام کاربری خود را وارد کنید" />
                        ) : null} */}

                        <Label htmlFor="pass">
                            <Flex jc="space-between" ai="center">
                                <div>رمز عبور</div>
                            </Flex>
                        </Label>
                        <Input
                            id="pass"
                            dir="ltr"
                            type="password"
                            onChange={(e) => setPass(e.target.value)}
                        />
                        {/* {checkErrors && !pass ? (
                            <InputInfo title="رمز عبور خود را وارد کنید" />
                        ) : null} */}

                        <Label htmlFor="answer">
                            <Flex jc="space-between" ai="center">
                            جواب سوال امنیتی
                            </Flex>
                        </Label>
                        <Input
                            id="answer"
                            dir="ltr"
                            textAlign="left"
                            onChange={(e) => setAnswer(e.target.value)}
                        />
                        {/* {checkErrors && !answer ? (
                            <InputInfo title="جواب سوال امنیتی خود را وارد کنید" />
                        ) : null} */}

                        <Flex ai="center">
                            <Button
                                onClick={changePassword}
                                loading={isLoading ? 1 : 0}
                                success
                                icon="checkmark"
                            >
                                تایید و ثبت
                            </Button>
                        </Flex>
                    </form>
                </div>
            </Card>
            <Flex jc="space-between" ai="center" className="switch-part">
                <Flex fd="column" jc="center" ai="flex-start">
                    <div className="switch-part__title">عضو تسکولو نیستید؟</div>
                    <div className="switch-part__subtitle">
                        به صورت رایگان ثبت نام کنید
          </div>
                </Flex>
                <Link to="/auth/register">
                    <Button success={1} invert={1} nomargin={1}>
                        ثبت نام
          </Button>
                </Link>
            </Flex>
        </Flex>
    );

}
const StyledForgetPass = styled(ForgetPass)`
    width: 100%;
    min-height: 100vh;
    /* margin-top: 30vh; */
    .switch-part {
        width: 100%;
        padding: 30px;
        box-sizing: border-box;
        font-family: ${CONSTS.fonts.normal};
        color: #000000;
        &__title {
        font-weight: 500;
        font-size: 16px;
        }
        &__subtitle {
        font-weight: 300;
        font-size: 12px;
        }
    }
`;
export default StyledForgetPass;
