import React from "react";
import styled from "styled-components";
import { Card, Button, Flex, GroupCard, ButtonGroup } from "../.././components";
import CONSTS from "../../utils/constants";
import ContentPartBg from "./../../static/images/auth-bg.jpg";
import AuthPartBg from "./../../static/images/auth-pattern-bg.jpg";
import AuthBorder from "./../../static/images/auth-border.svg";
import WhiteLogo from "./../../static/images/white-logo.png";
import Logo from "./../../static/images/taskaloo.svg";
import Login from "./Login";
import Register from "./Register";
import ForgetPass from "./ForgetPass";
// import RepairPass from "./RepairPass";
// import ConfirmEmail from "./ConfirmEmail";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
} from "react-router-dom";

function Auth({ className, ...props }) {
  let match = useRouteMatch();
  return (
    <Flex className={className} {...props}>
      
      <Flex fd="column" ai="center" jc="center" className="left-side">
        
        <Switch>
          <Route path={`${match.path}/forget-pass`}>
            <ForgetPass className="form-card" />
          </Route>
          <Route path={`${match.path}/register`}>
            <Register className="form-card" />
          </Route>
          <Route path={match.path}>
            <Login className="form-card" />
          </Route>
        </Switch>

      </Flex>
    </Flex>
  );
}

const StyledAuth = styled(Auth)`
  width: 100%;
  /* height: 100vh; */
  overflow-x: hidden;
  @media only screen and (max-width: ${CONSTS.responsive.lg}px) {
    flex-direction: column-reverse;
  }
  .left-side {
    width: 100%;
    height: 100%;
    /* background-image: url(${AuthPartBg}); */
    /* background-color: #00ab3f; */
    /* background-size: cover;
    background-position: center; */
    margin-right: auto;
    /* z-index: 111; */
    .form-card {
      width: 400px;
    }
    &__logo {
      display: none;
      &__img {
        background-image: url(${Logo});
        background-size: contain;
        background-position: center;
        background-repeat: no-repeat;
        width: 75px;
        height: 75px;
        margin-top: 30px;
      }
      &__owner {
        margin-top: 16px;
        font-family: ${CONSTS.fonts.normal};
        font-weight: 400;
        font-size: 15px;
        /* color: ${CONSTS.colors.primary.normal}; */
        color: #015a65;
      }
      &__title {
        margin-top: 5px;
        font-family: ${CONSTS.fonts.normal};
        font-weight: 500;
        font-size: 17px;
        color: ${CONSTS.colors.primary.normal};
      }
    }
    @media only screen and (max-width: ${CONSTS.responsive.lg}px) {
      width: calc(100% - 50px);
      margin-left: auto;
      margin-right: auto;
      border-radius: 0;
      min-height: 100vh;
      &__logo {
        display: flex;
      }
      .form-card {
        padding-top: 0;
        min-height: auto !important;
        width: 100%;
      }
    }
  }

  .right-side {
    background-image: url(${ContentPartBg});
    background-color: #00ab3f;
    background-size: cover;
    background-position: center;
    width: calc(100% - 700px);
    margin-left: auto;
    height: 100%;
    border-top-left-radius: 50px;
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    @media only screen and (max-width: ${CONSTS.responsive.lg}px) {
      position: relative;
      width: 100%;
      /* border-radius: 0; */
    }
    .auth-border {
      position: absolute;
      left: 40px;
      top: 40px;
      height: 93px;
      @media only screen and (max-width: ${CONSTS.responsive.lg}px) {
        height: 50px;
      }
    }
    .owner {
      /* margin-top: 100px; */
      position: absolute;
      top: 50px;
      right: 50px;
      @media only screen and (max-width: ${CONSTS.responsive.lg}px) {
        display: none;
      }
      &__logo {
        background-image: url(${WhiteLogo});
        background-size: contain;
        background-position: center;
        background-repeat: no-repeat;
        width: 100px;
        height: 100px;
      }
      &__title {
        margin-top: 16px;
        font-family: ${CONSTS.fonts.normal};
        font-weight: 500;
        font-size: 16px;
        color: #ffffff;
      }
    }
    .descriptions {
      /* max-width: 600px; */
      padding: 50px;
      box-sizing: border-box;
      margin-top: auto;
      &__title {
        font-family: ${CONSTS.fonts.normal};
        font-weight: 300;
        font-size: 31px;
        color: #ffffff;
        margin-bottom: 0;
        @media only screen and (max-width: ${CONSTS.responsive.lg}px) {
          font-size: 1.2rem;
        }
      }
      &__name {
        font-family: ${CONSTS.fonts.normal};
        font-weight: bold;
        font-size: 39px;
        color: #ffffff;
        margin-top: 0;
        @media only screen and (max-width: ${CONSTS.responsive.lg}px) {
          font-size: 1.3rem;
        }
      }
      p {
        font-family: ${CONSTS.fonts.normal};
        font-weight: 300;
        font-size: 20px;
        color: #ffffff;
        letter-spacing: 0;
        line-height: 33px;
        margin-top: 40px;
        @media only screen and (max-width: ${CONSTS.responsive.lg}px) {
          font-size: 1rem;
          line-height: 1.8rem;
        }
      }
    }
  }
  .links {
    margin-bottom: 50px;
    margin-right: 50px;
    @media only screen and (max-width: ${CONSTS.responsive.lg}px) {
      flex-direction: column;
      width: 100%;
      margin-right: auto;
      margin-left: auto;
      &__in {
        align-items: center;
        flex-direction: column;
      }
    }
  }
`;

export default StyledAuth;
