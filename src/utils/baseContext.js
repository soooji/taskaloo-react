import React from "react";

export const navbar = {
  isOpen: false,
};

export const NavBarContext = React.createContext(navbar.isOpen);
