// import iran from "iran"; //used for provinces and cities
import _ from "lodash";
import jalaali from "jalaali-js";
import moment from "moment-jalaali";
import fa from "moment/locale/fa";
import Axios from "axios";
import CONSTS from "./constants";
import { toast } from "react-toastify";
import * as Yup from "yup";
import { useLocation } from "react-router-dom";

moment.locale("fa", fa);
moment.loadPersian({ dialect: "persian-modern" });



//Get user object from localstorage
export async function getCachedUser() {
  let user = localStorage.getItem("user");
  if (user) {
    let userObject = JSON.parse(user);
    return userObject;
  } else {
    window.location.replace("/auth");
  }
}


//Convert api query object to url query slug (string)
export function queryToString(query) {
  if (Object.keys(query).length == 0) return "";

  var str = "?";
  for (var key in query) {
    if (query.hasOwnProperty(key)) {
      if (query[key] != null || query[key] != undefined) {
        str += key + "=";
        str += query[key];
        if (key != Object.keys(query)[Object.keys(query).length - 1]) {
          str += "&";
        }
      }
    }
  }
  return str;
}

//default api requests error handler
export const errorHandler = (e, ignoreStatusEffect = false) => {
  console.log("RRRRR", e?.response);
  if (e?.response?.data?.message && CONSTS?.terms?.error) {
    let errtxt = CONSTS.terms.error;
    if (e?.response?.data?.message?.text) {
      errtxt = e?.response?.data?.message?.text;
    } else if (typeof e?.response?.data?.message?.details == String) {
      errtxt = e?.response?.data?.message?.details;
    } else if (Object.keys(e?.response?.data?.message?.details)?.length > 0) {
      errtxt = e?.response?.data?.message?.details[Object.keys(e?.response?.data?.message?.details)[0]][0];
    }

    toast(
      errtxt, {
      type: toast.TYPE.ERROR,
    }
    );
  } else {
    toast(CONSTS?.terms?.unhandled, {
      type: toast.TYPE.ERROR,
    });
  }
  if (
    !ignoreStatusEffect &&
    e?.response?.status == 403 &&
    e?.response?.status == 401
  ) {
    window.location.replace("/auth");
  }
};
//default api requests error handler
export const successHandler = (method) => {
  let str = "";
  switch (method) {
    case "get":
      return;
    case "put":
      str = "تغییرات ذخیره شد!";
      break;
    case "post":
      str = "مورد جدید با موفقیت ایجاد شد!";
      break;
    case "delete":
      str = "مورد با موفقیت حذف شد!";
      break;
    default:
      return;
  }
  toast(str, { type: toast.TYPE.SUCCESS });
};


//normal api request method generator
let tokenExceptions = [""]; //token will not be added to these url methods

export function apiMethodGenerator(
  url,
  reqMethod = "get",
  muteSuccessToast = false
) {
  const method = async (query, urlExtension) => {
    let res = null;
    let err = null;

    let token = localStorage.getItem("token");
    if (!tokenExceptions.includes(url)) {
      if (!token) {
        window.location.replace("/auth");
        throw {
          error: "TOKEN",
        };
      }
    }

    let reqHasBodyData = reqMethod == "put" || reqMethod == "post";

    let final_url = `${CONSTS.api}${url ?? ""}${urlExtension ?? ""}${query && !reqHasBodyData
      ? typeof query == "object"
        ? queryToString({ ...query })
        : "/" + query + ""
      : ""
      }`;
    final_url += `${final_url.includes("?") ? "&" : "?"
      }secret_token=${JSON.parse(token)}`;
    await Axios({
      method: reqMethod,
      url: final_url,
      ...(reqHasBodyData ? { data: query } : {}), //Here, 'query' is actually data field (for 'put' and 'post')
    })
      .then((r) => {
        res = r.data;
        if (!muteSuccessToast) {
          successHandler(reqMethod);
        }
        if (
          typeof Storage !== "undefined" &&
          !tokenExceptions.includes(url) &&
          r?.data?.token
        ) {
          localStorage.setItem("token", JSON.stringify(r.data.token));
        }
      })
      .catch((e) => {
        errorHandler(e);
        console.log(e);
        err = e;
      });
    if (res) {
      return res;
    } else {
      throw err;
    }
  };
  return method;
}

