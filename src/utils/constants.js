export const api = "http://techniture.ir/api/v1";
// export const baseMedia = "http://api.feeddss.rqi.ir";

export const colors = {
  primary: {
    light: "rgba(0,171,63,0.10)",
    midLight: "rgba(0,171,63,0.40)",
    normal: "#00AB3F",
    dark: "#02742C",
  },
  info: {
    light: "rgba(35,152,255,0.10)",
    midLight: "rgba(35,152,255,0.40)",
    normal: "#2398FF",
    dark: "#045DAC",
  },
  error: {
    light: "rgba(255,116,112,0.10)",
    midLight: "rgba(255,116,112,0.40)",
    normal: "#FF7470",
    dark: "#CF3E3A",
  },
  success: {
    light: "rgba(37,196,119,0.10)",
    midLight: "rgba(37,196,119,0.40)",
    normal: "#25C477",
    dark: "#067E44",
  },
  warning: {
    light: "rgba(222,194,36,0.10)",
    midLight: "rgba(222,194,36,0.40)",
    normal: "#DEC224",
    dark: "#998304",
  },
  grey: [
    "#0B0C0D", // 0
    "#212529", // 1
    "#343A40", // 2
    "#495057", // 3
    "#6C757D", // 4
    "#ADB5BD", // 5
    "#CED4DA", // 6
    "#DEE2E6", // 7
    "#E9ECEF", // 8 - border color
    "#F6F8FA", // 9
  ],
};

export const borderRadius = {
  inside: "3px",
  normal: "9px",
  outside: "15px",
};

export const responsive = {
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
};

export const terms = {
  error: "خطایی رخ داده، دوباره تلاش کنید یا با پشتیبانی در ارتباط باشید.",
  removeConfirm: {
    title: "آیا از حذف این مورد اطمینان دارید؟",
    message: "توجه داشته باشید که هیچ کدام یک از موارد، پس از تایید حذف قابل بازیابی نیستند."
  },
  unhandled: "خطایی مدیریت نشده رخ داده، با پشتیبانی تماس بگیرید.",
  form: {
    required: "مقدار فیلد بالا را وارد کنید.",
    min: (num)=>`مقدار این فیلد باید بیشتر از ${num} باشد!`,
    max: (num)=>`مقدار این فیلد باید کمتر از ${num} باشد!`,
    hasError: "مقادیر فرم‌ها به درستی وارد نشده"
  }
};

export const fonts = {
  normal: "PeydaWeb",
};
export default { colors, borderRadius, responsive, fonts, terms ,api};
