import React, { useState, useEffect } from "react";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import styled from "styled-components";
import "./App.css";
import "./static/fonts/peyda/stylesheet.css";
import "./static/fontIcons/css/jiredaam.css";
import "./static/css/select-search.css";
import "react-tabs/style/react-tabs.css";
// import "swiper/swiper.scss";
import "react-responsive-modal/styles.css";
import "react-toastify/dist/ReactToastify.css";
import noScroll from "no-scroll";
import { useIsOnline } from "react-use-is-online";
import { ToastContainer, toast } from "react-toastify";
import Auth from "./containers/auth";
import { PrivateRoute, ScrollToTop, Header, NavBar } from './components';
import CONSTS from "./utils/constants"
import Dashboard from "./containers/dashboard"
import Projects from "./containers/projects"
import Users from "./containers/users"
import ChangePassword from "./containers/change_pass"
import Profile from "./containers/users/Profile"
import Stats from "./containers/stats"
import { NavBarContext } from "./utils/baseContext";

function App({ className }) {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const {
    // isOnline,
    isOffline,
    // error
  } = useIsOnline(); //check net status
  useEffect(() => {
    isMenuOpen ? noScroll.on() : noScroll.off();
  }, [isMenuOpen]);

  useEffect(() => {
    if (isOffline) {
      toast("اتصال اینترنت خود را بررسی کنید.", {
        autoClose: false,
        type: toast.TYPE.ERROR,
      });
    } else {
      toast.dismiss();
    }
  }, [isOffline]);
  // useEffect(() => {
  //   var myHeaders = new Headers();
  //   // myHeaders.append(
  //     // "Cookie",
  //     // "connect.sid=s%3Ac673d60b-1fba-4d00-b34e-3f1131866cb0.CqUfhrs3qAYCKaX3wMApMWEi5kg1nqh4NDOflThEqNs"
  //   // );

  //   var requestOptions = {
  //     method: "GET",
  //     headers: myHeaders,
  //     // redirect: "follow",
  //   };

  //   fetch("http://5.253.24.115/api/v1/project", requestOptions)
  //     .then((response) => response.text())
  //     .then((result) => console.log(result))
  //     .catch((error) => console.log("error", error));
  // }, []);
  return (
    <NavBarContext.Provider
      value={{
        isMenuOpen: isMenuOpen,
        toggleMenu: (newState) =>
          setIsMenuOpen(newState != null ? newState : !isMenuOpen),
      }}
    >
      <div className={className}>
        <ToastContainer
          rtl
          className="toast-cmp toast-cmp__container"
          toastClassName="toast-cmp toast-cmp__toast"
          bodyClassName="toast-cmp toast-cmp__body"
          position="bottom-center"
        />
        <Router>
          <ScrollToTop />
          <Switch>
            <Route path="/auth">
              <Auth />
            </Route>
            <PrivateRoute path="/">
              <Header className="noprint" />
              <NavBar className="noprint" />
              <div className="page-container">
                <Switch>
                  <Route path="/stats">
                    <Stats />
                  </Route>
                  <Route path="/profile">
                    <Profile />
                  </Route>
                  <Route path="/change_password">
                    <ChangePassword />
                  </Route>
                  <Route path="/users">
                    <Users />
                  </Route>
                  <Route path="/projects">
                    <Projects />
                  </Route>
                  <Route path="/">
                    <Dashboard />
                  </Route>
                </Switch>
              </div>
            </PrivateRoute>
          </Switch>
        </Router>
      </div>
    </NavBarContext.Provider>
  );
}
const StyledApp = styled(App)`
  overflow-x: hidden;
  .page-container {
    overflow-x: hidden;
    width: calc(100% - 240px);
    margin-right: auto;
    margin-top: 40px;
    padding: 35px;
    box-sizing: border-box;
    @media only screen and (max-width: ${CONSTS.responsive.md}px) {
      width: 100%;
      padding: 20px 10px;
    }
    @media print {
      width: 100% !important;
      margin-top: 0 !important;
      padding: 0 !important;
    }
  }
`;
export default StyledApp;
