/* eslint-disable import/no-anonymous-default-export */
import { apiMethodGenerator } from "../utils/functions";
const getUser = apiMethodGenerator("/user/all");
const putUser = apiMethodGenerator("/user/all","put");
const getUserProfile = apiMethodGenerator("/user/profile");
const putUserProfile = apiMethodGenerator("/user/profile", "put");



export default {
    getUser,
    getUserProfile,
    putUserProfile,
    putUser
};
