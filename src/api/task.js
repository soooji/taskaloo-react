/* eslint-disable import/no-anonymous-default-export */
import { apiMethodGenerator } from "../utils/functions";
const getTasks = apiMethodGenerator("/task");
const putTask = apiMethodGenerator("/task", "put");
const postTask = apiMethodGenerator("/task", "post");
const deleteTask = apiMethodGenerator("/task", "delete");
const getTags = apiMethodGenerator("/tag");
const postTag = apiMethodGenerator("/tag", "post");
const postComment = apiMethodGenerator("/task", "post");
const deleteComment = apiMethodGenerator("/task/comment", "delete");

export default {
  getTasks,
  putTask,
  postTask,
  deleteTask,
  getTags,
  postTag,
  postComment,
  deleteComment,
};
