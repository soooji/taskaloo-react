import { apiMethodGenerator } from "../utils/functions";
const getStats = apiMethodGenerator("/stat");

export default {
    getStats
};