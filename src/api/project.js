/* eslint-disable import/no-anonymous-default-export */
import { apiMethodGenerator } from "../utils/functions";
const getProjects = apiMethodGenerator("/project");
const putProject = apiMethodGenerator("/project", "put");
const postProject = apiMethodGenerator("/project", "post");
const deleteProject = apiMethodGenerator("/project", "delete");

export default {
  getProjects,
  putProject,
  postProject,
  deleteProject,
};
