import Axios from "axios";
import CONSTS from "../utils/constants";
import { errorHandler, apiMethodGenerator } from "../utils/functions";

const register = async (data) => {
  let res = null;
  let err = null;
  await Axios.post(`${CONSTS.api}/auth/register`, data)
    .then((r) => {
      res = r.data;
    })
    .catch((e) => {
      errorHandler(e, true);
      console.log(e);
      err = e;
    });
  if (res) {
    return res;
  } else {
    throw err;
  }
};
const forgetPass = async (data) => {
  let res = null;
  let err = null;
  await Axios.post(`${CONSTS.api}/auth/forgot-password`,{...data})
    .then((r) => {
      res = r.data;
    })
    .catch((e) => {
      errorHandler(e, true);
      console.log(e);
      err = e;
    });
  if (res) {
    return res;
  } else {
    throw err;
  }
};
const login = async (data) => {
  let res = null;
  let err = null;
  await Axios.post(`${CONSTS.api}/auth/login`, data)
    .then((r) => {
      if (typeof Storage !== "undefined") {
        if (!r?.data?.id || !r?.data?.token) {
          throw { error: "USER" };
        }
        let userToLocale = r.data;
        localStorage.setItem("token", JSON.stringify(r.data.token));
        delete userToLocale.token;
        localStorage.setItem("user", JSON.stringify(userToLocale));
      }
      res = r.data;
    })
    .catch((e) => {
      errorHandler(e, true);
      console.log(e);
      err = e;
    });
  if (res) {
    return res;
  } else {
    throw err;
  }
};
export default {
  Register: register,
  Login: login,
  ForgetPass: forgetPass
};
